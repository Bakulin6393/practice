﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using Microsoft.Office.Interop.Excel;

namespace practice
{
    public partial class Form9 : Form
    {
        List<string> files = new List<string>();
        FolderBrowserDialog fold = new FolderBrowserDialog();

        public Form9()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            fold = new FolderBrowserDialog();
            fold.ShowDialog();

            if (fold.SelectedPath == "")
            {
                return;
            }

            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 40)
            {
                label2.Text = label2.Text.Substring(0, 40) + "...";
            }
        }

        private void Form9_Load(object sender, EventArgs e)
        {
            FileStream r = new FileStream("регионы.txt", FileMode.OpenOrCreate, FileAccess.Read);
            TextReader r_txt = new StreamReader(r);
            string buf;
            bool f = true;
            while (f)
            {
                try
                {
                    buf = r_txt.ReadLine();
                    comboBox3.Items.Add(buf);
                }
                catch
                {
                    f = false;
                }
            }
            r_txt.Close();
            r.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label2.Text = "";
            files.Clear();
            richTextBox1.Clear();
            numericUpDown1.Value = 0;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            XDocument doc = new XDocument();
            if (fold.SelectedPath == "")
            {
                return;
            }
            string[] otchety = Directory.GetFiles(fold.SelectedPath, "*.xml");

            for (int i = 0; i < otchety.Length; i++)
            {
                try
                {
                    doc = XDocument.Load(otchety[i]);
                    if (richTextBox1.Text == "")
                    {
                        richTextBox1.AppendText(doc.Element("company").Element("attributes").Element("name").Value);
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + doc.Element("company").Element("attributes").Element("name").Value);
                    }
                    files.Add(otchety[i]);
                    numericUpDown1.Value += 1;
                }
                catch
                {
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > 0)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }

        private String IsInReestr(TextReader r_txt, string id)
        {
            bool res = false;
            string s;
            bool f = true;
            while (f)
            {
                try
                {
                    s = r_txt.ReadLine();
                    if (s.Substring(0, s.IndexOf('-')) == id)
                    {
                        return s;
                    }
                }
                catch
                {
                    f = false;
                }
            }
            return null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application excel_otchet = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excel_otchet.Workbooks.Open(Directory.GetCurrentDirectory()+"\\Компании.xls");
            Worksheet ws = wb.Sheets[1];
            XDocument doc = new XDocument();
            string short_name;
            string date;
            string buf;
            int kv=0;
            FileStream read = new FileStream("реестр.txt",FileMode.OpenOrCreate,FileAccess.Read);
            TextReader r_txt = new StreamReader(read);
            string id;
            for (int i = 0; i < files.Count; i++)
            {
                try
                {
                    doc = XDocument.Load(files[i]);
                    id = IsInReestr(r_txt, doc.Element("company").Attribute("id").Value);
                    if (id==null)
                    {
                        continue;
                    }
                    date = doc.Element("company").Attribute("period").Value;
                    buf = date.Substring(date.LastIndexOf('.') + 1);//год
                    date = date.Remove(date.LastIndexOf('.'));//не год
                    date = date.Remove(0, date.IndexOf('.') + 1);
                    switch (comboBox1.Text)
                    {
                        case "Первый квартал": if (kv > 3)
                            {
                                continue;
                            }
                            break;

                        case "Первое полугодие": if (kv > 6)
                            {
                                continue;
                            }
                            break;

                        case "Девять месяцев": if (kv > 9)
                            {
                                continue;
                            }
                            break;
                    }
                    ws.Cells[i + 5, 1] = i + 1;
                    ws.Cells[i + 5, 2] = doc.Element("company").Attribute("id").Value;
                    id = id.Substring(id.IndexOf('-')+1);
                    ws.Cells[i + 5, 3] = id.Substring(0,id.IndexOf('('));
                    ws.Cells[i + 5, 4] = "Региональная СО";
                    id=id.Substring(id.IndexOf('(')+1);
                    ws.Cells[i + 5, 5] = id.Substring(0,id.IndexOf(','));
                    id = id.Substring(id.IndexOf(',')+1);
                    ws.Cells[i + 5, 7] = id.Substring(0,id.IndexOf(','));
                    id = id.Substring(id.IndexOf(',')+1);
                    ws.Cells[i + 5, 6] = id.Substring(0,id.IndexOf(')'));
                    ws.Cells[i + 5, 8] = 0;
                    ws.Cells[i + 5, 9] = 0;
                        
                }
                catch
                {
                }
                
                int ind = 4 + files.Count;
                Range r = ws.get_Range("A" + 5, "I" + ind);
                r.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                r.WrapText = true;
                r.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                r.VerticalAlignment = XlVAlign.xlVAlignCenter;
            }

            fold.ShowDialog();
            if (fold.SelectedPath == "")
            {
                wb.Close(SaveChanges: false);
                excel_otchet.Quit();
                wb = null;
                ws = null;
                excel_otchet = null;
                GC.Collect();
                return;
            }
            wb.SaveAs(fold.SelectedPath + "\\Компании на территории "+comboBox3.Text+".xls");
            wb.Close(SaveChanges:false);
            excel_otchet.Quit();
            wb = null;
            ws = null;
            excel_otchet = null;
            GC.Collect();
        }
    }
}
