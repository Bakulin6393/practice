﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace practice
{
    public partial class Form55 : Form
    {
        public Form55()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 40)
            {
                label2.Text = label2.Text.Substring(0, 40) + "...";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label8.Text = fold.SelectedPath;
            if (label8.Text.Length > 40)
            {
                label8.Text = label8.Text.Substring(0, 40) + "...";
            }
        }
    }
}
