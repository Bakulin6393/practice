﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace practice
{
    public partial class reestr : Form
    {
        public reestr()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void reestr_Load(object sender, EventArgs e)
        {
            FileStream r = new FileStream("реестр.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            TextReader r_txt = new StreamReader(r);
            bool f = true;
            string[] str_array = new string[5];
            string s = null;
            int ind = 0;
            int ind2 = 0;
            int i = 1;
            while (f)
            {
                try
                {
                    s = r_txt.ReadLine();
                    ind = s.IndexOf('-');
                    str_array[0] = s.Substring(0, ind);
                    ind2 = ind+1;
                    ind = s.LastIndexOf('(') - ind - 1;
                    str_array[1] = s.Substring(ind2, ind);
                    ind2 = ind;
                    str_array[4] = s.Substring(s.LastIndexOf(',')+1, s.LastIndexOf(')') - s.LastIndexOf(',')-1);
                    s = s.Remove(s.LastIndexOf(','));
                    str_array[3] = s.Substring(s.LastIndexOf(',') + 1);
                    str_array[2] = s.Substring(s.LastIndexOf('(')+1, s.LastIndexOf(',') - s.LastIndexOf('(')-1);

                    dataGridView1.Rows.Add(i, str_array[0], str_array[1], str_array[2], str_array[3], str_array[4]);
                    i++;
                    
                }
                catch
                {
                    f = false;
                    r_txt.Close();
                    r.Close();
                }
            }
            r_txt.Close();
            r.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            long id;
            if ((Int64.TryParse(textBox1.Text, out id)))
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text.Length > 0)
            {
                button2.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            if (textBox1.Text != "")
            {
                for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
                {
                    if (dataGridView1[1, i].Value.ToString() == textBox1.Text)
                    {
                        cell = dataGridView1[0, i];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.Rows[i].Selected = true;
                        break;
                    }
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!((textBox4.Text.Length > 0) && (textBox5.Text.Length > 0) && (textBox6.Text.Length > 0) && (textBox7.Text.Length > 0)))
            {
                label8.Text = "Заполните все поля перед добавлением записи";
                return;
            }

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1[1, i].Value.ToString() == textBox3.Text)
                {
                    label8.Text = "Запись с таким рег. номером уже существует";
                    return;
                }
            }
            
            dataGridView1.Rows.Add(dataGridView1.Rows.Count, textBox3.Text, textBox4.Text.ToUpper(), textBox5.Text, textBox6.Text, textBox7.Text);
            dataGridView1.Sort(dataGridView1.Columns[2], ListSortDirection.Ascending);
            for (int i = 1; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1[0, i-1].Value = i;
            }
            label8.Text = "";
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            long id;
            if ((Int64.TryParse(textBox3.Text, out id)))
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            if (textBox2.Text != "")
            {
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    if (dataGridView1[2, i].Value.ToString() == textBox2.Text.ToUpper())
                    {
                        cell = dataGridView1[0, i];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.Rows[i].Selected = true;
                        break;
                    }
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int row_id = -1;
            if (dataGridView1.SelectedCells.Count > 0)
            {
                row_id = dataGridView1.SelectedCells[0].RowIndex;
            }
            else
            {
                return;
            }
            dataGridView1.Rows.RemoveAt(row_id);
            for (int i = 1; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1[0, i - 1].Value = i;
            }
            label8.Text = "";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FileStream w = new FileStream("реестр.txt", FileMode.Create, FileAccess.Write);
            TextWriter w_txt = new StreamWriter(w);
            string buf = null;
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                buf = dataGridView1[1, i].Value + "-" + dataGridView1[2, i].Value + "(" + dataGridView1[3, i].Value + "," +
                    dataGridView1[4, i].Value + "," + dataGridView1[5, i].Value + ")";
                w_txt.WriteLine(buf);
            }
            w_txt.Close();
            w.Close();
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
