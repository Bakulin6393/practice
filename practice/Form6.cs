﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace practice
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label7.Text = fold.SelectedPath;
            if (label7.Text.Length > 30)
            {
                label7.Text = label7.Text.Substring(0, 30) + "...";
            }
        }

        private void Form6_Load(object sender, EventArgs e)
        {

        }
    }
}
