﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace practice
{
    public partial class Form7 : Form
    {
        FileDialog dial;

        public Form7()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void textBox41_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox38_TextChanged(object sender, EventArgs e)
        {

        }

        private void label115_Click(object sender, EventArgs e)
        {

        }

        public void button2_Click(object sender, EventArgs e)
        {
            
            dial.ShowDialog();
            label115.Text = dial.FileName;
            if (dial.FileName.Substring(dial.FileName.LastIndexOf('.')+1)!="xml")
            {
                label115.Text = "Выберите xml файл";
                return;
            }
            if (label115.Text.Length > 30)
            {
                label115.Text = label115.Text.Substring(0, 30) + "...";
            }

            XmlTextReader reader = new XmlTextReader(dial.FileName);
            XDocument file = XDocument.Load(dial.FileName);
            textBox1.Text = file.Element("company").Attribute("id").Value;
            textBox2.Text = file.Element("company").Element("attributes").Element("name").Value;
            textBox3.Text = file.Element("company").Element("attributes").Element("address").Value;
            textBox4.Text = file.Element("company").Element("attributes").Element("head").Element("name").Value;
            textBox5.Text = file.Element("company").Element("attributes").Element("assistant").Element("name").Value;
            textBox6.Text = file.Element("company").Element("attributes").Element("phone").Value;
            textBox7.Text = file.Element("company").Element("attributes").Element("e-mail").Value;
            textBox9.Text = file.Element("company").Element("attributes").Element("date").Value;
            textBox10.Text = file.Element("company").Element("attributes").Element("okato").Value;
            textBox11.Text = file.Element("company").Element("attributes").Element("okfs").Value;
            textBox12.Text = file.Element("company").Element("attributes").Element("okopf").Value;
            textBox13.Text = file.Element("company").Element("attributes").Element("okpo").Value;
            textBox14.Text = file.Element("company").Element("attributes").Element("okved").Value;

            IEnumerable<XElement> elems = file.Element("company").Element("form").Elements("table");
            IEnumerator<XElement> num = elems.GetEnumerator();
            richTextBox1.Clear();
            while (num.MoveNext())
            {
                richTextBox1.AppendText(num.Current.Attribute("id").Value+"\n");
            }

            textBox15.Text = file.Element("company").Element("form").Element("table").Attribute("id").Value;
        }

        private void Form7_Load(object sender, EventArgs e)
        {
            dial = new OpenFileDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
