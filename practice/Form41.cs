﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace practice
{
    public partial class Form41 : Form
    {
        public Form41()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 40)
            {
                label2.Text = label2.Text.Substring(0, 40) + "...";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {

            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label13.Text = fold.SelectedPath;
            if (label13.Text.Length > 40)
            {
                label13.Text = label13.Text.Substring(0, 40) + "...";
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label3.Text = fold.SelectedPath;
            if (label3.Text.Length > 40)
            {
                label3.Text = label3.Text.Substring(0, 40) + "...";
            }
        }
    }
}
