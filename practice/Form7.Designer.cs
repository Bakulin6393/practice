﻿namespace practice
{
    partial class Form7
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.label82 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.label100 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.label112 = new System.Windows.Forms.Label();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label115 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(209, 197);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(810, 311);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.textBox55);
            this.tabPage1.Controls.Add(this.textBox54);
            this.tabPage1.Controls.Add(this.textBox53);
            this.tabPage1.Controls.Add(this.textBox52);
            this.tabPage1.Controls.Add(this.textBox51);
            this.tabPage1.Controls.Add(this.textBox50);
            this.tabPage1.Controls.Add(this.textBox49);
            this.tabPage1.Controls.Add(this.textBox48);
            this.tabPage1.Controls.Add(this.textBox47);
            this.tabPage1.Controls.Add(this.label56);
            this.tabPage1.Controls.Add(this.label55);
            this.tabPage1.Controls.Add(this.label54);
            this.tabPage1.Controls.Add(this.label53);
            this.tabPage1.Controls.Add(this.label52);
            this.tabPage1.Controls.Add(this.label51);
            this.tabPage1.Controls.Add(this.label50);
            this.tabPage1.Controls.Add(this.label49);
            this.tabPage1.Controls.Add(this.label48);
            this.tabPage1.Controls.Add(this.textBox46);
            this.tabPage1.Controls.Add(this.textBox45);
            this.tabPage1.Controls.Add(this.textBox44);
            this.tabPage1.Controls.Add(this.textBox43);
            this.tabPage1.Controls.Add(this.textBox42);
            this.tabPage1.Controls.Add(this.textBox41);
            this.tabPage1.Controls.Add(this.textBox40);
            this.tabPage1.Controls.Add(this.textBox39);
            this.tabPage1.Controls.Add(this.label47);
            this.tabPage1.Controls.Add(this.label46);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.label44);
            this.tabPage1.Controls.Add(this.label43);
            this.tabPage1.Controls.Add(this.label42);
            this.tabPage1.Controls.Add(this.label41);
            this.tabPage1.Controls.Add(this.label40);
            this.tabPage1.Controls.Add(this.textBox38);
            this.tabPage1.Controls.Add(this.textBox37);
            this.tabPage1.Controls.Add(this.textBox36);
            this.tabPage1.Controls.Add(this.textBox35);
            this.tabPage1.Controls.Add(this.textBox34);
            this.tabPage1.Controls.Add(this.textBox33);
            this.tabPage1.Controls.Add(this.textBox32);
            this.tabPage1.Controls.Add(this.textBox31);
            this.tabPage1.Controls.Add(this.textBox30);
            this.tabPage1.Controls.Add(this.textBox29);
            this.tabPage1.Controls.Add(this.textBox28);
            this.tabPage1.Controls.Add(this.label39);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.label37);
            this.tabPage1.Controls.Add(this.label36);
            this.tabPage1.Controls.Add(this.label35);
            this.tabPage1.Controls.Add(this.label34);
            this.tabPage1.Controls.Add(this.label33);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.textBox27);
            this.tabPage1.Controls.Add(this.textBox26);
            this.tabPage1.Controls.Add(this.textBox25);
            this.tabPage1.Controls.Add(this.textBox24);
            this.tabPage1.Controls.Add(this.textBox23);
            this.tabPage1.Controls.Add(this.textBox22);
            this.tabPage1.Controls.Add(this.textBox21);
            this.tabPage1.Controls.Add(this.textBox20);
            this.tabPage1.Controls.Add(this.textBox19);
            this.tabPage1.Controls.Add(this.textBox18);
            this.tabPage1.Controls.Add(this.textBox17);
            this.tabPage1.Controls.Add(this.textBox16);
            this.tabPage1.Controls.Add(this.textBox15);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label17);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.comboBox2);
            this.tabPage1.Controls.Add(this.comboBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(802, 285);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Сведения о стаховой деятельности";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // textBox55
            // 
            this.textBox55.Location = new System.Drawing.Point(663, 232);
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new System.Drawing.Size(113, 20);
            this.textBox55.TabIndex = 110;
            // 
            // textBox54
            // 
            this.textBox54.Location = new System.Drawing.Point(585, 232);
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new System.Drawing.Size(62, 20);
            this.textBox54.TabIndex = 109;
            // 
            // textBox53
            // 
            this.textBox53.Location = new System.Drawing.Point(487, 232);
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new System.Drawing.Size(92, 20);
            this.textBox53.TabIndex = 108;
            // 
            // textBox52
            // 
            this.textBox52.Location = new System.Drawing.Point(419, 232);
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(65, 20);
            this.textBox52.TabIndex = 107;
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(336, 232);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(77, 20);
            this.textBox51.TabIndex = 106;
            // 
            // textBox50
            // 
            this.textBox50.Location = new System.Drawing.Point(262, 232);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(73, 20);
            this.textBox50.TabIndex = 105;
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(186, 232);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(72, 20);
            this.textBox49.TabIndex = 104;
            // 
            // textBox48
            // 
            this.textBox48.Location = new System.Drawing.Point(119, 232);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(61, 20);
            this.textBox48.TabIndex = 103;
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.Location = new System.Drawing.Point(15, 232);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(95, 20);
            this.textBox47.TabIndex = 102;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(660, 216);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(116, 13);
            this.label56.TabIndex = 101;
            this.label56.Text = "Иное ОС(кроме ОМС)";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(585, 216);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(31, 13);
            this.label55.TabIndex = 100;
            this.label55.Text = "ОПО";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(484, 216);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(76, 13);
            this.label54.TabIndex = 99;
            this.label54.Text = "Перевозч. ВС";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(427, 216);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(38, 13);
            this.label53.TabIndex = 98;
            this.label53.Text = "Осаго";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(337, 216);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(51, 13);
            this.label52.TabIndex = 97;
            this.label52.Text = "Военных";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(267, 216);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(38, 13);
            this.label51.TabIndex = 96;
            this.label51.Text = "Налог";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(189, 216);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(58, 13);
            this.label50.TabIndex = 95;
            this.label50.Text = "Пациенты";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(120, 216);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(50, 13);
            this.label49.TabIndex = 94;
            this.label49.Text = "Пассаж.";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label48.Location = new System.Drawing.Point(12, 216);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(88, 13);
            this.label48.TabIndex = 93;
            this.label48.Text = "О.С. без ОМС";
            // 
            // textBox46
            // 
            this.textBox46.Location = new System.Drawing.Point(678, 184);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(98, 20);
            this.textBox46.TabIndex = 92;
            // 
            // textBox45
            // 
            this.textBox45.Location = new System.Drawing.Point(575, 184);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(90, 20);
            this.textBox45.TabIndex = 91;
            // 
            // textBox44
            // 
            this.textBox44.Location = new System.Drawing.Point(469, 184);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(100, 20);
            this.textBox44.TabIndex = 90;
            // 
            // textBox43
            // 
            this.textBox43.Location = new System.Drawing.Point(385, 184);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(78, 20);
            this.textBox43.TabIndex = 89;
            // 
            // textBox42
            // 
            this.textBox42.Location = new System.Drawing.Point(253, 184);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(123, 20);
            this.textBox42.TabIndex = 88;
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(165, 184);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(82, 20);
            this.textBox41.TabIndex = 87;
            this.textBox41.TextChanged += new System.EventHandler(this.textBox41_TextChanged);
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(85, 184);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(76, 20);
            this.textBox40.TabIndex = 86;
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(15, 184);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(65, 20);
            this.textBox39.TabIndex = 85;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.Location = new System.Drawing.Point(675, 168);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(65, 13);
            this.label47.TabIndex = 84;
            this.label47.Text = "Фин.риск";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(572, 168);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(87, 13);
            this.label46.TabIndex = 83;
            this.label46.Text = "Предпр. риск";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(466, 168);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(100, 13);
            this.label45.TabIndex = 82;
            this.label45.Text = "Обяз. по договору";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(382, 168);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(73, 13);
            this.label44.TabIndex = 81;
            this.label44.Text = "Вред 3лицам";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(250, 168);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(126, 13);
            this.label43.TabIndex = 80;
            this.label43.Text = "Недост. товаров, работ";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(173, 168);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(60, 13);
            this.label42.TabIndex = 79;
            this.label42.Text = "Экспл. ОО";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(82, 168);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(79, 13);
            this.label41.TabIndex = 78;
            this.label41.Text = "Вл.водн.транс";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 168);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(64, 13);
            this.label40.TabIndex = 77;
            this.label40.Text = "Вл.возд.тр.";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(717, 135);
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new System.Drawing.Size(59, 20);
            this.textBox38.TabIndex = 76;
            this.textBox38.TextChanged += new System.EventHandler(this.textBox38_TextChanged);
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(629, 135);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(79, 20);
            this.textBox37.TabIndex = 75;
            // 
            // textBox36
            // 
            this.textBox36.Location = new System.Drawing.Point(564, 135);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(59, 20);
            this.textBox36.TabIndex = 74;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(503, 135);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(56, 20);
            this.textBox35.TabIndex = 73;
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(397, 135);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(100, 20);
            this.textBox34.TabIndex = 72;
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(297, 135);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(94, 20);
            this.textBox33.TabIndex = 71;
            // 
            // textBox32
            // 
            this.textBox32.Location = new System.Drawing.Point(235, 135);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(56, 20);
            this.textBox32.TabIndex = 70;
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(172, 135);
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(57, 20);
            this.textBox31.TabIndex = 69;
            // 
            // textBox30
            // 
            this.textBox30.Location = new System.Drawing.Point(123, 135);
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(43, 20);
            this.textBox30.TabIndex = 68;
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(70, 135);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(47, 20);
            this.textBox29.TabIndex = 67;
            // 
            // textBox28
            // 
            this.textBox28.Location = new System.Drawing.Point(15, 135);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(49, 20);
            this.textBox28.TabIndex = 66;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(714, 114);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(62, 13);
            this.label39.TabIndex = 65;
            this.label39.Text = "Вл. ж/д тр.";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(626, 114);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(75, 13);
            this.label38.TabIndex = 64;
            this.label38.Text = "в т.ч. Зел.кар";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(561, 114);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(60, 13);
            this.label37.TabIndex = 63;
            this.label37.Text = "Вл.автотр.";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(500, 114);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(59, 13);
            this.label36.TabIndex = 62;
            this.label36.Text = "Ответ-ть";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(394, 114);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(100, 13);
            this.label35.TabIndex = 61;
            this.label35.Text = "Пр. им-во физ.лиц";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(294, 114);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(94, 13);
            this.label34.TabIndex = 60;
            this.label34.Text = "Пр. им-во юр.лиц";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(232, 114);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(55, 13);
            this.label33.TabIndex = 59;
            this.label33.Text = "Субсидии";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(173, 114);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(60, 13);
            this.label32.TabIndex = 58;
            this.label32.Text = "в т.ч. с г/п";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(124, 114);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 13);
            this.label31.TabIndex = 57;
            this.label31.Text = "С/Х";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(67, 114);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(38, 13);
            this.label30.TabIndex = 56;
            this.label30.Text = "Грузы";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(12, 114);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(49, 13);
            this.label29.TabIndex = 55;
            this.label29.Text = "Водн.тр.";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(671, 80);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(54, 20);
            this.textBox27.TabIndex = 54;
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(616, 80);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(55, 20);
            this.textBox26.TabIndex = 53;
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(552, 80);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(64, 20);
            this.textBox25.TabIndex = 52;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(487, 80);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(63, 20);
            this.textBox24.TabIndex = 51;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(449, 80);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(35, 20);
            this.textBox23.TabIndex = 50;
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(405, 80);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(38, 20);
            this.textBox22.TabIndex = 49;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(349, 80);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(54, 20);
            this.textBox21.TabIndex = 48;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(305, 80);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(42, 20);
            this.textBox20.TabIndex = 47;
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(262, 80);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(37, 20);
            this.textBox19.TabIndex = 46;
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(211, 80);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(47, 20);
            this.textBox18.TabIndex = 45;
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(156, 80);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(49, 20);
            this.textBox17.TabIndex = 44;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(108, 80);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(42, 20);
            this.textBox16.TabIndex = 43;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(15, 80);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(91, 20);
            this.textBox15.TabIndex = 42;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(668, 61);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(46, 13);
            this.label28.TabIndex = 41;
            this.label28.Text = "Возд.тр";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(613, 61);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 13);
            this.label27.TabIndex = 40;
            this.label27.Text = "Ж/Д тр.";
            this.label27.Click += new System.EventHandler(this.label27_Click);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(549, 61);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(58, 13);
            this.label26.TabIndex = 39;
            this.label26.Text = "Назем.тр.";
            this.label26.Click += new System.EventHandler(this.label26_Click);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(484, 61);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(59, 13);
            this.label25.TabIndex = 38;
            this.label25.Text = "Имущ-во";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(446, 61);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 13);
            this.label24.TabIndex = 37;
            this.label24.Text = "ДМС";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(416, 61);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 36;
            this.label23.Text = "НС";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(353, 61);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 35;
            this.label22.Text = "Личное";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(302, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 13);
            this.label21.TabIndex = 34;
            this.label21.Text = "Пенсия";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(259, 61);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(37, 13);
            this.label20.TabIndex = 33;
            this.label20.Text = "Рента";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(208, 61);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Смерть";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(155, 61);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 31;
            this.label18.Text = "Жизнь";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(116, 61);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(33, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "Д.С.";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(12, 61);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "Всего без ОМС";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(12, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "Показатель:";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Россия",
            "Центральный ФО",
            "Северо-Западный ФО",
            "Южный ФО",
            "Приволжский ФО",
            "Юго-Восточный регион",
            "Волго-Камский регион",
            "ПриФО",
            "Уральский ФО",
            "Сибирский ФО",
            "Дальневосточный ФО",
            "Северо-Кавказский ФО",
            "Республика Адыгея",
            "Республика Башкортостан",
            "Республика Бурятия",
            "Республика Алтай",
            "Республика Дагестан",
            "Республика Ингушетия",
            "Кабардино-Балкарская Республика",
            "Республика Калмыкия",
            "Республика Карачаево-Черкессия",
            "Республика Карелия",
            "Республика Коми",
            "Республика Марий Эл",
            "Республика Мордовия",
            "Республика Саха (Якутия)",
            "Республика Северная Осетия-Алания",
            "Республика Татарстан",
            "Республика Тыва",
            "Удмуртская Республика",
            "Республика Хакасия",
            "Чувашская Республика",
            "Алтайский край",
            "Краснодарский край",
            "Красноярский край",
            "Приморский край",
            "Ставропольский край",
            "Хабаровский край",
            "Амурская область",
            "Архангельская область",
            "Астраханская область",
            "Белгородская область",
            "Брянская область",
            "Владимирская область",
            "Волгоградская область",
            "Вологодская область",
            "Воронежская область",
            "Ивановская область",
            "Иркутская область",
            "Калиниградская область",
            "Калужская область",
            "Камчатская область",
            "Кемеровская область",
            "Кировская область",
            "Костромская область",
            "Курганская область",
            "Курская область",
            "Ленинградская область",
            "Липецкая область",
            "Магаданская область",
            "Московская область",
            "Мурманская область",
            "Нижегородская область",
            "Новгородская область",
            "Новосибирская область",
            "Омская область",
            "Оренбургская область",
            "Орловская область",
            "Пензенская область",
            "Пермская область",
            "Псковская область",
            "Ростовская область",
            "Рязанская область",
            "Самарская область",
            "Саратовская область",
            "Сахалинская область",
            "Свердловская область",
            "Смоленская область",
            "Тамбовская область",
            "Тверская область",
            "Томская область",
            "Тульская область",
            "Тюменская область",
            "Ульяновская область",
            "Челябинская область",
            "Читинская область",
            "Ярославская область",
            "г. Москва",
            "г. Санкт-Петербург",
            "Еврейская автономная область",
            "Агинский Бурятский авт. округ",
            "Коми-Пермяцкий автономный округ",
            "Корякский автономный округ",
            "Ненецкий автономный округ",
            "Таймырский (Долгано-Ненецкий) автономный округ",
            "Усть-Ордынский Бурятский автономный округ",
            "Ханты-Мансийский автономный округ",
            "Чукотский автономный округ",
            "Эвенкийский автономный округ",
            "Ямало-Ненецкий автономный округ",
            "Чеченская Республика"});
            this.comboBox2.Location = new System.Drawing.Point(537, 29);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(239, 21);
            this.comboBox2.TabIndex = 27;
            this.comboBox2.Text = "Алтайский край";
            this.comboBox2.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(15, 29);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(492, 21);
            this.comboBox1.TabIndex = 26;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.textBox78);
            this.tabPage2.Controls.Add(this.textBox77);
            this.tabPage2.Controls.Add(this.textBox76);
            this.tabPage2.Controls.Add(this.label82);
            this.tabPage2.Controls.Add(this.label81);
            this.tabPage2.Controls.Add(this.label80);
            this.tabPage2.Controls.Add(this.textBox75);
            this.tabPage2.Controls.Add(this.textBox74);
            this.tabPage2.Controls.Add(this.textBox73);
            this.tabPage2.Controls.Add(this.textBox72);
            this.tabPage2.Controls.Add(this.label79);
            this.tabPage2.Controls.Add(this.label78);
            this.tabPage2.Controls.Add(this.label77);
            this.tabPage2.Controls.Add(this.label76);
            this.tabPage2.Controls.Add(this.label75);
            this.tabPage2.Controls.Add(this.textBox71);
            this.tabPage2.Controls.Add(this.textBox70);
            this.tabPage2.Controls.Add(this.textBox69);
            this.tabPage2.Controls.Add(this.label74);
            this.tabPage2.Controls.Add(this.label73);
            this.tabPage2.Controls.Add(this.label72);
            this.tabPage2.Controls.Add(this.textBox68);
            this.tabPage2.Controls.Add(this.textBox67);
            this.tabPage2.Controls.Add(this.textBox66);
            this.tabPage2.Controls.Add(this.textBox65);
            this.tabPage2.Controls.Add(this.textBox64);
            this.tabPage2.Controls.Add(this.textBox63);
            this.tabPage2.Controls.Add(this.label71);
            this.tabPage2.Controls.Add(this.label70);
            this.tabPage2.Controls.Add(this.label69);
            this.tabPage2.Controls.Add(this.label68);
            this.tabPage2.Controls.Add(this.label67);
            this.tabPage2.Controls.Add(this.label66);
            this.tabPage2.Controls.Add(this.textBox62);
            this.tabPage2.Controls.Add(this.textBox61);
            this.tabPage2.Controls.Add(this.textBox60);
            this.tabPage2.Controls.Add(this.textBox59);
            this.tabPage2.Controls.Add(this.textBox58);
            this.tabPage2.Controls.Add(this.textBox57);
            this.tabPage2.Controls.Add(this.textBox56);
            this.tabPage2.Controls.Add(this.label65);
            this.tabPage2.Controls.Add(this.label64);
            this.tabPage2.Controls.Add(this.label63);
            this.tabPage2.Controls.Add(this.label62);
            this.tabPage2.Controls.Add(this.label61);
            this.tabPage2.Controls.Add(this.label60);
            this.tabPage2.Controls.Add(this.label59);
            this.tabPage2.Controls.Add(this.label58);
            this.tabPage2.Controls.Add(this.comboBox4);
            this.tabPage2.Controls.Add(this.comboBox3);
            this.tabPage2.Controls.Add(this.label57);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(802, 285);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Сведения о деят-ти мед.организации";
            // 
            // textBox78
            // 
            this.textBox78.Location = new System.Drawing.Point(398, 254);
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new System.Drawing.Size(397, 20);
            this.textBox78.TabIndex = 50;
            // 
            // textBox77
            // 
            this.textBox77.Location = new System.Drawing.Point(185, 252);
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new System.Drawing.Size(185, 20);
            this.textBox77.TabIndex = 49;
            // 
            // textBox76
            // 
            this.textBox76.Location = new System.Drawing.Point(9, 254);
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new System.Drawing.Size(149, 20);
            this.textBox76.TabIndex = 48;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(395, 238);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(353, 13);
            this.label82.TabIndex = 47;
            this.label82.Text = "Оплата медпомощи, оказанной застрахованным медорганизациям";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(182, 238);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(191, 13);
            this.label81.TabIndex = 46;
            this.label81.Text = "Кол-во застр. на конец отч. периода";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 238);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(152, 13);
            this.label80.TabIndex = 45;
            this.label80.Text = "Кол-во застр. в отч. периоде";
            // 
            // textBox75
            // 
            this.textBox75.Location = new System.Drawing.Point(623, 215);
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new System.Drawing.Size(172, 20);
            this.textBox75.TabIndex = 44;
            // 
            // textBox74
            // 
            this.textBox74.Location = new System.Drawing.Point(514, 215);
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(100, 20);
            this.textBox74.TabIndex = 43;
            // 
            // textBox73
            // 
            this.textBox73.Location = new System.Drawing.Point(291, 215);
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new System.Drawing.Size(216, 20);
            this.textBox73.TabIndex = 42;
            // 
            // textBox72
            // 
            this.textBox72.Location = new System.Drawing.Point(9, 215);
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new System.Drawing.Size(255, 20);
            this.textBox72.TabIndex = 41;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(620, 199);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(105, 13);
            this.label79.TabIndex = 40;
            this.label79.Text = "от юр. и физич. лиц";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(510, 199);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(73, 13);
            this.label78.TabIndex = 39;
            this.label78.Text = "от медорг-ий";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(288, 199);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(98, 13);
            this.label77.TabIndex = 38;
            this.label77.Text = "из них от ТФОМС";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 199);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(264, 13);
            this.label76.TabIndex = 37;
            this.label76.Text = "Средства для оплаты медпомощи, причитающ. СО";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(6, 186);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(40, 13);
            this.label75.TabIndex = 36;
            this.label75.Text = "II.ОМС";
            // 
            // textBox71
            // 
            this.textBox71.Location = new System.Drawing.Point(301, 163);
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new System.Drawing.Size(66, 20);
            this.textBox71.TabIndex = 35;
            // 
            // textBox70
            // 
            this.textBox70.Location = new System.Drawing.Point(164, 163);
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(100, 20);
            this.textBox70.TabIndex = 34;
            // 
            // textBox69
            // 
            this.textBox69.Location = new System.Drawing.Point(9, 163);
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new System.Drawing.Size(136, 20);
            this.textBox69.TabIndex = 33;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(298, 147);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(74, 13);
            this.label74.TabIndex = 32;
            this.label74.Text = "из-за рубежа";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(162, 147);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(88, 13);
            this.label73.TabIndex = 31;
            this.label73.Text = "из них в России";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(6, 147);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(141, 13);
            this.label72.TabIndex = 30;
            this.label72.Text = "Доля перестр. в выплатах";
            // 
            // textBox68
            // 
            this.textBox68.Location = new System.Drawing.Point(702, 124);
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new System.Drawing.Size(93, 20);
            this.textBox68.TabIndex = 29;
            // 
            // textBox67
            // 
            this.textBox67.Location = new System.Drawing.Point(575, 124);
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new System.Drawing.Size(118, 20);
            this.textBox67.TabIndex = 28;
            // 
            // textBox66
            // 
            this.textBox66.Location = new System.Drawing.Point(443, 124);
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new System.Drawing.Size(123, 20);
            this.textBox66.TabIndex = 27;
            // 
            // textBox65
            // 
            this.textBox65.Location = new System.Drawing.Point(301, 124);
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new System.Drawing.Size(135, 20);
            this.textBox65.TabIndex = 26;
            // 
            // textBox64
            // 
            this.textBox64.Location = new System.Drawing.Point(165, 124);
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new System.Drawing.Size(120, 20);
            this.textBox64.TabIndex = 25;
            // 
            // textBox63
            // 
            this.textBox63.Location = new System.Drawing.Point(9, 124);
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(136, 20);
            this.textBox63.TabIndex = 24;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(699, 108);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(42, 13);
            this.label71.TabIndex = 23;
            this.label71.Text = "прочие";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(572, 108);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(121, 13);
            this.label70.TabIndex = 22;
            this.label70.Text = "из них страх. выплаты";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(440, 108);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(126, 13);
            this.label69.TabIndex = 21;
            this.label69.Text = "Выплаты по договорам";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(298, 108);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(138, 13);
            this.label68.TabIndex = 20;
            this.label68.Text = "из них отказов в выплате";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(161, 108);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(131, 13);
            this.label67.TabIndex = 19;
            this.label67.Text = "Кол-во урег. страх. случ.";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(6, 108);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(139, 13);
            this.label66.TabIndex = 18;
            this.label66.Text = "Кол-во заявл. страх. случ.";
            // 
            // textBox62
            // 
            this.textBox62.Location = new System.Drawing.Point(623, 85);
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new System.Drawing.Size(172, 20);
            this.textBox62.TabIndex = 17;
            // 
            // textBox61
            // 
            this.textBox61.Location = new System.Drawing.Point(493, 85);
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new System.Drawing.Size(124, 20);
            this.textBox61.TabIndex = 16;
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(388, 85);
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new System.Drawing.Size(93, 20);
            this.textBox60.TabIndex = 15;
            // 
            // textBox59
            // 
            this.textBox59.Location = new System.Drawing.Point(329, 85);
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(53, 20);
            this.textBox59.TabIndex = 14;
            // 
            // textBox58
            // 
            this.textBox58.Location = new System.Drawing.Point(237, 85);
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new System.Drawing.Size(88, 20);
            this.textBox58.TabIndex = 13;
            // 
            // textBox57
            // 
            this.textBox57.Location = new System.Drawing.Point(118, 85);
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new System.Drawing.Size(105, 20);
            this.textBox57.TabIndex = 12;
            // 
            // textBox56
            // 
            this.textBox56.Location = new System.Drawing.Point(9, 85);
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new System.Drawing.Size(100, 20);
            this.textBox56.TabIndex = 11;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(620, 69);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(175, 13);
            this.label65.TabIndex = 10;
            this.label65.Text = "Кол-во действ. на конец периода";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(487, 69);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(127, 13);
            this.label64.TabIndex = 9;
            this.label64.Text = "Кол-во закл. договоров";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(385, 69);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(96, 13);
            this.label63.TabIndex = 8;
            this.label63.Text = "Страховая сумма";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(326, 69);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(53, 13);
            this.label62.TabIndex = 7;
            this.label62.Text = "за рубеж";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(234, 69);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(90, 13);
            this.label61.TabIndex = 6;
            this.label61.Text = "из них в Россию";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(115, 69);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(113, 13);
            this.label60.TabIndex = 5;
            this.label60.Text = "Передано в перестр.";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 69);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(103, 13);
            this.label59.TabIndex = 4;
            this.label59.Text = "Страховые премии";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(6, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 13);
            this.label58.TabIndex = 3;
            this.label58.Text = "I. ДМС";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Россия",
            "Центральный ФО",
            "Северо-Западный ФО",
            "Южный ФО",
            "Приволжский ФО",
            "Юго-Восточный регион",
            "Волго-Камский регион",
            "ПриФО",
            "Уральский ФО",
            "Сибирский ФО",
            "Дальневосточный ФО",
            "Северо-Кавказский ФО",
            "Республика Адыгея",
            "Республика Башкортостан",
            "Республика Бурятия",
            "Республика Алтай",
            "Республика Дагестан",
            "Республика Ингушетия",
            "Кабардино-Балкарская Республика",
            "Республика Калмыкия",
            "Республика Карачаево-Черкессия",
            "Республика Карелия",
            "Республика Коми",
            "Республика Марий Эл",
            "Республика Мордовия",
            "Республика Саха (Якутия)",
            "Республика Северная Осетия-Алания",
            "Республика Татарстан",
            "Республика Тыва",
            "Удмуртская Республика",
            "Республика Хакасия",
            "Чувашская Республика",
            "Алтайский край",
            "Краснодарский край",
            "Красноярский край",
            "Приморский край",
            "Ставропольский край",
            "Хабаровский край",
            "Амурская область",
            "Архангельская область",
            "Астраханская область",
            "Белгородская область",
            "Брянская область",
            "Владимирская область",
            "Волгоградская область",
            "Вологодская область",
            "Воронежская область",
            "Ивановская область",
            "Иркутская область",
            "Калиниградская область",
            "Калужская область",
            "Камчатская область",
            "Кемеровская область",
            "Кировская область",
            "Костромская область",
            "Курганская область",
            "Курская область",
            "Ленинградская область",
            "Липецкая область",
            "Магаданская область",
            "Московская область",
            "Мурманская область",
            "Нижегородская область",
            "Новгородская область",
            "Новосибирская область",
            "Омская область",
            "Оренбургская область",
            "Орловская область",
            "Пензенская область",
            "Пермская область",
            "Псковская область",
            "Ростовская область",
            "Рязанская область",
            "Самарская область",
            "Саратовская область",
            "Сахалинская область",
            "Свердловская область",
            "Смоленская область",
            "Тамбовская область",
            "Тверская область",
            "Томская область",
            "Тульская область",
            "Тюменская область",
            "Ульяновская область",
            "Челябинская область",
            "Читинская область",
            "Ярославская область",
            "г. Москва",
            "г. Санкт-Петербург",
            "Еврейская автономная область",
            "Агинский Бурятский авт. округ",
            "Коми-Пермяцкий автономный округ",
            "Корякский автономный округ",
            "Ненецкий автономный округ",
            "Таймырский (Долгано-Ненецкий) автономный округ",
            "Усть-Ордынский Бурятский автономный округ",
            "Ханты-Мансийский автономный округ",
            "Чукотский автономный округ",
            "Эвенкийский автономный округ",
            "Ямало-Ненецкий автономный округ",
            "Чеченская Республика"});
            this.comboBox4.Location = new System.Drawing.Point(558, 22);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(237, 21);
            this.comboBox4.TabIndex = 2;
            this.comboBox4.Text = "Алтайский край";
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "Всего",
            "  Из них по договорам страхования с физическими лицами",
            "  Из них по договорам страхования с юридическими лицами",
            "  Из них на территории России",
            "      Из них по региону",
            "  Из них за пределами России",
            "Суммы, начисленные по регрессивным требованиям",
            "Суммы, полученные по регрессивным требованиям"});
            this.comboBox3.Location = new System.Drawing.Point(6, 22);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(513, 21);
            this.comboBox3.TabIndex = 1;
            this.comboBox3.Text = "Всего";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label57.Location = new System.Drawing.Point(6, 3);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(82, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Показатель:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.textBox94);
            this.tabPage3.Controls.Add(this.textBox93);
            this.tabPage3.Controls.Add(this.label100);
            this.tabPage3.Controls.Add(this.label99);
            this.tabPage3.Controls.Add(this.textBox92);
            this.tabPage3.Controls.Add(this.textBox91);
            this.tabPage3.Controls.Add(this.label98);
            this.tabPage3.Controls.Add(this.label97);
            this.tabPage3.Controls.Add(this.textBox90);
            this.tabPage3.Controls.Add(this.textBox89);
            this.tabPage3.Controls.Add(this.textBox88);
            this.tabPage3.Controls.Add(this.textBox87);
            this.tabPage3.Controls.Add(this.label96);
            this.tabPage3.Controls.Add(this.label95);
            this.tabPage3.Controls.Add(this.label94);
            this.tabPage3.Controls.Add(this.label93);
            this.tabPage3.Controls.Add(this.label92);
            this.tabPage3.Controls.Add(this.textBox86);
            this.tabPage3.Controls.Add(this.textBox85);
            this.tabPage3.Controls.Add(this.textBox84);
            this.tabPage3.Controls.Add(this.textBox83);
            this.tabPage3.Controls.Add(this.label91);
            this.tabPage3.Controls.Add(this.label90);
            this.tabPage3.Controls.Add(this.label89);
            this.tabPage3.Controls.Add(this.label88);
            this.tabPage3.Controls.Add(this.textBox82);
            this.tabPage3.Controls.Add(this.textBox81);
            this.tabPage3.Controls.Add(this.textBox80);
            this.tabPage3.Controls.Add(this.textBox79);
            this.tabPage3.Controls.Add(this.label87);
            this.tabPage3.Controls.Add(this.label86);
            this.tabPage3.Controls.Add(this.label85);
            this.tabPage3.Controls.Add(this.label84);
            this.tabPage3.Controls.Add(this.label83);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(802, 285);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "ПВУ";
            // 
            // textBox94
            // 
            this.textBox94.Location = new System.Drawing.Point(491, 243);
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new System.Drawing.Size(305, 20);
            this.textBox94.TabIndex = 33;
            // 
            // textBox93
            // 
            this.textBox93.Location = new System.Drawing.Point(9, 243);
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new System.Drawing.Size(471, 20);
            this.textBox93.TabIndex = 32;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(488, 222);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(254, 13);
            this.label100.TabIndex = 31;
            this.label100.Text = "Сумма выплат потерп. в счет осущ-ого ими ПВУ";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 222);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(476, 13);
            this.label99.TabIndex = 30;
            this.label99.Text = "Сумма выплат по ОСАГО , признанных на основании предст. страховщ. потерп. требова" +
    "ний";
            // 
            // textBox92
            // 
            this.textBox92.Location = new System.Drawing.Point(456, 199);
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(340, 20);
            this.textBox92.TabIndex = 29;
            // 
            // textBox91
            // 
            this.textBox91.Location = new System.Drawing.Point(9, 199);
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new System.Drawing.Size(438, 20);
            this.textBox91.TabIndex = 28;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(453, 183);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(316, 13);
            this.label98.TabIndex = 27;
            this.label98.Text = "Кол-во треб. об оплате, по которым признана сумма выплат";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(6, 183);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(441, 13);
            this.label97.TabIndex = 26;
            this.label97.Text = "Сумма выплат, произв. страховщ. потерп. по ПВУ в соотв. с треб. страховщ. потерп." +
    "";
            // 
            // textBox90
            // 
            this.textBox90.Location = new System.Drawing.Point(537, 160);
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new System.Drawing.Size(259, 20);
            this.textBox90.TabIndex = 25;
            // 
            // textBox89
            // 
            this.textBox89.Location = new System.Drawing.Point(407, 160);
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new System.Drawing.Size(119, 20);
            this.textBox89.TabIndex = 24;
            // 
            // textBox88
            // 
            this.textBox88.Location = new System.Drawing.Point(231, 160);
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new System.Drawing.Size(167, 20);
            this.textBox88.TabIndex = 23;
            // 
            // textBox87
            // 
            this.textBox87.Location = new System.Drawing.Point(9, 160);
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new System.Drawing.Size(213, 20);
            this.textBox87.TabIndex = 22;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(532, 144);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(213, 13);
            this.label96.TabIndex = 21;
            this.label96.Text = "Кол-во требований от страховщ. потерп.";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(404, 144);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(122, 13);
            this.label95.TabIndex = 20;
            this.label95.Text = "Кол-во мотив. отказов";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(228, 144);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(170, 13);
            this.label94.TabIndex = 19;
            this.label94.Text = "Сумма предполагаемых выплат";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(6, 144);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(216, 13);
            this.label93.TabIndex = 18;
            this.label93.Text = "Кол-во предв. увед. от страховщ. потерп.";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(6, 119);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(181, 13);
            this.label92.TabIndex = 17;
            this.label92.Text = "II. Страховщик причинителя вреда";
            // 
            // textBox86
            // 
            this.textBox86.Location = new System.Drawing.Point(615, 82);
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new System.Drawing.Size(181, 20);
            this.textBox86.TabIndex = 16;
            // 
            // textBox85
            // 
            this.textBox85.Location = new System.Drawing.Point(383, 82);
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(223, 20);
            this.textBox85.TabIndex = 15;
            // 
            // textBox84
            // 
            this.textBox84.Location = new System.Drawing.Point(139, 82);
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new System.Drawing.Size(228, 20);
            this.textBox84.TabIndex = 14;
            // 
            // textBox83
            // 
            this.textBox83.Location = new System.Drawing.Point(11, 82);
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new System.Drawing.Size(119, 20);
            this.textBox83.TabIndex = 13;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(612, 66);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(141, 13);
            this.label91.TabIndex = 12;
            this.label91.Text = "Сумма поступивших денег";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(380, 66);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(226, 13);
            this.label90.TabIndex = 11;
            this.label90.Text = "Кол-во треб. по которым поступили деньги";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(136, 66);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(238, 13);
            this.label89.TabIndex = 10;
            this.label89.Text = "Кол-во треб. направл. страховщикам виновн.";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(8, 66);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(122, 13);
            this.label88.TabIndex = 9;
            this.label88.Text = "Сумма выплат потерп.";
            // 
            // textBox82
            // 
            this.textBox82.Location = new System.Drawing.Point(499, 43);
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new System.Drawing.Size(297, 20);
            this.textBox82.TabIndex = 8;
            // 
            // textBox81
            // 
            this.textBox81.Location = new System.Drawing.Point(307, 43);
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(177, 20);
            this.textBox81.TabIndex = 7;
            // 
            // textBox80
            // 
            this.textBox80.Location = new System.Drawing.Point(187, 43);
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new System.Drawing.Size(111, 20);
            this.textBox80.TabIndex = 6;
            // 
            // textBox79
            // 
            this.textBox79.Location = new System.Drawing.Point(9, 43);
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new System.Drawing.Size(169, 20);
            this.textBox79.TabIndex = 5;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(496, 27);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(221, 13);
            this.label87.TabIndex = 4;
            this.label87.Text = "Количество опл. требований потерпевших";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(304, 27);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(176, 13);
            this.label86.TabIndex = 3;
            this.label86.Text = "Количество мот. отказов потерп.";
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(184, 27);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(114, 13);
            this.label85.TabIndex = 2;
            this.label85.Text = "Сумма предп.выплат";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 27);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(172, 13);
            this.label84.TabIndex = 1;
            this.label84.Text = "Кол-во требований потерпевших";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 3);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(153, 13);
            this.label83.TabIndex = 0;
            this.label83.Text = "I. Страховщик потерпевшего";
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.textBox103);
            this.tabPage4.Controls.Add(this.textBox102);
            this.tabPage4.Controls.Add(this.textBox101);
            this.tabPage4.Controls.Add(this.label112);
            this.tabPage4.Controls.Add(this.textBox100);
            this.tabPage4.Controls.Add(this.textBox99);
            this.tabPage4.Controls.Add(this.textBox98);
            this.tabPage4.Controls.Add(this.textBox97);
            this.tabPage4.Controls.Add(this.textBox96);
            this.tabPage4.Controls.Add(this.textBox95);
            this.tabPage4.Controls.Add(this.label111);
            this.tabPage4.Controls.Add(this.label110);
            this.tabPage4.Controls.Add(this.label109);
            this.tabPage4.Controls.Add(this.label108);
            this.tabPage4.Controls.Add(this.label107);
            this.tabPage4.Controls.Add(this.label106);
            this.tabPage4.Controls.Add(this.label105);
            this.tabPage4.Controls.Add(this.label104);
            this.tabPage4.Controls.Add(this.label103);
            this.tabPage4.Controls.Add(this.label102);
            this.tabPage4.Controls.Add(this.label101);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(802, 285);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Число членов ОВС";
            // 
            // textBox103
            // 
            this.textBox103.Location = new System.Drawing.Point(227, 248);
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new System.Drawing.Size(100, 20);
            this.textBox103.TabIndex = 20;
            // 
            // textBox102
            // 
            this.textBox102.Location = new System.Drawing.Point(108, 248);
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new System.Drawing.Size(100, 20);
            this.textBox102.TabIndex = 19;
            // 
            // textBox101
            // 
            this.textBox101.Location = new System.Drawing.Point(9, 248);
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new System.Drawing.Size(93, 20);
            this.textBox101.TabIndex = 18;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(6, 194);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(760, 13);
            this.label112.TabIndex = 17;
            this.label112.Text = "III. Число членов, прекративших членство, но несущих субсидарную ответ-ть по стра" +
    "ховым обязательствам общества, на конец отчетного периода";
            // 
            // textBox100
            // 
            this.textBox100.Location = new System.Drawing.Point(227, 151);
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new System.Drawing.Size(100, 20);
            this.textBox100.TabIndex = 16;
            // 
            // textBox99
            // 
            this.textBox99.Location = new System.Drawing.Point(108, 151);
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new System.Drawing.Size(100, 20);
            this.textBox99.TabIndex = 15;
            // 
            // textBox98
            // 
            this.textBox98.Location = new System.Drawing.Point(9, 151);
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new System.Drawing.Size(93, 20);
            this.textBox98.TabIndex = 14;
            // 
            // textBox97
            // 
            this.textBox97.Location = new System.Drawing.Point(227, 60);
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new System.Drawing.Size(100, 20);
            this.textBox97.TabIndex = 13;
            // 
            // textBox96
            // 
            this.textBox96.Location = new System.Drawing.Point(108, 60);
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(100, 20);
            this.textBox96.TabIndex = 12;
            // 
            // textBox95
            // 
            this.textBox95.Location = new System.Drawing.Point(9, 60);
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new System.Drawing.Size(93, 20);
            this.textBox95.TabIndex = 11;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(6, 106);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(235, 13);
            this.label111.TabIndex = 10;
            this.label111.Text = "II. Число членов на конец отчетного периода";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(224, 223);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(95, 13);
            this.label110.TabIndex = 9;
            this.label110.Text = "Юридических лиц";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(105, 223);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(91, 13);
            this.label109.TabIndex = 8;
            this.label109.Text = "Физических лиц";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(6, 223);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(37, 13);
            this.label108.TabIndex = 7;
            this.label108.Text = "Всего";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(224, 135);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(95, 13);
            this.label107.TabIndex = 6;
            this.label107.Text = "Юридических лиц";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(105, 135);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(91, 13);
            this.label106.TabIndex = 5;
            this.label106.Text = "Физических лиц";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(6, 135);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(37, 13);
            this.label105.TabIndex = 4;
            this.label105.Text = "Всего";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(224, 44);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(95, 13);
            this.label104.TabIndex = 3;
            this.label104.Text = "Юридических лиц";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(105, 44);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(91, 13);
            this.label103.TabIndex = 2;
            this.label103.Text = "Физических лиц";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(6, 44);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(37, 13);
            this.label102.TabIndex = 1;
            this.label102.Text = "Всего";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(6, 21);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(292, 13);
            this.label101.TabIndex = 0;
            this.label101.Text = "I. Число членов, вступивших в ОВС, за отчетный период";
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.textBox104);
            this.tabPage5.Controls.Add(this.label113);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(802, 285);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Операции, принятые РФМ";
            // 
            // textBox104
            // 
            this.textBox104.Location = new System.Drawing.Point(9, 39);
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new System.Drawing.Size(152, 20);
            this.textBox104.TabIndex = 1;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(6, 12);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(758, 13);
            this.label113.TabIndex = 0;
            this.label113.Text = "Количество операций, подлежащих обязательному контролю, сообщения о которых приня" +
    "ты Федеральной службой по финансовому мониторингу";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 462);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(168, 29);
            this.button1.TabIndex = 0;
            this.button1.Text = "Закрыть";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox14);
            this.groupBox1.Controls.Add(this.textBox13);
            this.groupBox1.Controls.Add(this.textBox12);
            this.groupBox1.Controls.Add(this.textBox11);
            this.groupBox1.Controls.Add(this.textBox10);
            this.groupBox1.Controls.Add(this.textBox9);
            this.groupBox1.Controls.Add(this.textBox8);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(209, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(739, 179);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Общие сведения об организации";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(589, 153);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 20);
            this.textBox14.TabIndex = 25;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(423, 153);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 24;
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(274, 153);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 23;
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(137, 153);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 22;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(6, 153);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 21;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(541, 111);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(186, 20);
            this.textBox9.TabIndex = 20;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(6, 111);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(505, 20);
            this.textBox8.TabIndex = 19;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(541, 71);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(186, 20);
            this.textBox7.TabIndex = 18;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(380, 72);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(131, 20);
            this.textBox6.TabIndex = 17;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(146, 71);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(225, 20);
            this.textBox5.TabIndex = 16;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(6, 71);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(134, 20);
            this.textBox4.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(380, 32);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(347, 20);
            this.textBox3.TabIndex = 15;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(112, 32);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(262, 20);
            this.textBox2.TabIndex = 14;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(6, 32);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 13;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(609, 134);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(45, 13);
            this.label14.TabIndex = 12;
            this.label14.Text = "ОКВЭД";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(450, 134);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "ОКПО";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(292, 134);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "ОКОПФ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(166, 134);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "ОКФС";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(29, 134);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "ОКАТО";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(538, 95);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Дата составления отчета";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(162, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Виды страховой деятельности";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(538, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "E-mail";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(377, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Контактный телефон";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(157, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ответственный за составление отчета";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Руководитель";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(431, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(146, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Адрес страховой компании";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(124, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Название страховой компании";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Рег.номер";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(9, 12);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(112, 13);
            this.label114.TabIndex = 1;
            this.label114.Text = "Выберите компанию";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 41);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(191, 39);
            this.button2.TabIndex = 2;
            this.button2.Text = "Выбрать компанию";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(12, 91);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(0, 13);
            this.label115.TabIndex = 3;
            this.label115.Click += new System.EventHandler(this.label115_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(12, 123);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(191, 325);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // Form7
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1031, 520);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form7";
            this.Text = "Создание отчета по выбранной компании";
            this.Load += new System.EventHandler(this.Form7_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.RichTextBox richTextBox1;
    }
}