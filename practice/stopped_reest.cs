﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace practice
{
    public partial class stopped_reest : Form
    {
        public stopped_reest()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void stopped_reest_Load(object sender, EventArgs e)
        {
            FileStream r = new FileStream("лицензии.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            TextReader r_txt = new StreamReader(r);
            bool f = true;
            string[] str_array = new string[4];
            string s;
            s = r_txt.ReadLine();
            if (s != "ОТОЗВАНЫ")
            {
                r_txt.Close();
                r.Close();
                return;
            }
            int i =1;
            while (f)
            {
                try
                {
                    s = r_txt.ReadLine();
                    if (s == "ПРИОСТАНОВЛЕНЫ")
                    {
                        f = false;
                    }
                    else
                    {
                        str_array[0] = s.Substring(0, s.IndexOf('-'));
                        s = s.Remove(0, s.IndexOf('-') + 1);
                        str_array[1] = s.Substring(0, s.LastIndexOf('('));
                        s = s.Remove(0, s.LastIndexOf('(') + 1);
                        str_array[2] = s.Substring(0, s.LastIndexOf(','));
                        s = s.Remove(0, s.LastIndexOf(',') + 1);
                        str_array[3] = s.Substring(0, s.LastIndexOf(')'));
                        dataGridView1.Rows.Add(i, str_array[0], str_array[1], str_array[2], str_array[3]);
                        i++;
                    }
                }
                catch
                {
                    f = false;
                }
            }

            if (s != "ПРИОСТАНОВЛЕНЫ")
            {
                r_txt.Close();
                r.Close();
                return;
            }

            f = true;
            i = 1;
            while (f)
            {
                try
                {
                    s = r_txt.ReadLine();

                    str_array[0] = s.Substring(0, s.IndexOf('-'));
                    s = s.Remove(0, s.IndexOf('-') + 1);
                    str_array[1] = s.Substring(0, s.LastIndexOf('('));
                    s = s.Remove(0, s.LastIndexOf('(') + 1);
                    str_array[2] = s.Substring(0, s.LastIndexOf(','));
                    s = s.Remove(0, s.LastIndexOf(',') + 1);
                    str_array[3] = s.Substring(0, s.LastIndexOf(')'));
                    dataGridView2.Rows.Add(i, str_array[0], str_array[1], str_array[2], str_array[3]);
                    i++;

                }
                catch
                {
                    f = false;
                }
            }
            r_txt.Close();
            r.Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            long id;
            if ((Int64.TryParse(textBox1.Text, out id)))
            {
                button1.Enabled = true;
            }
            else
            {
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            if (textBox1.Text != "")
            {
                for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                {
                    if (dataGridView1[1, i].Value.ToString() == textBox1.Text)
                    {
                        cell = dataGridView1[0, i];
                        dataGridView1.CurrentCell = cell;
                        dataGridView1.Rows[i].Selected = true;
                        break;
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((textBox2.Text == "") || (textBox3.Text == "") || (textBox4.Text == ""))
            {
                return;
            }

            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                if (dataGridView1[1, i].Value.ToString() == textBox4.Text)
                {
                    label11.Text = "Запись с таким рег. номером уже существует";
                    return;
                }
            }

            dataGridView1.Rows.Add(dataGridView1.Rows.Count, textBox4.Text, textBox3.Text.ToUpper(), textBox2.Text, dateTimePicker1.Value.ToShortDateString());
            dataGridView1.Sort(dataGridView1.Columns[2], ListSortDirection.Ascending);
            for (int i = 1; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1[0, i - 1].Value = i;
            }
            label11.Text = "";
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int row_id = -1;
            if (dataGridView1.SelectedCells.Count > 0)
            {
                row_id = dataGridView1.SelectedCells[0].RowIndex;
            }
            else
            {
                return;
            }
            dataGridView1.Rows.RemoveAt(row_id);
            for (int i = 1; i < dataGridView1.Rows.Count; i++)
            {
                dataGridView1[0, i - 1].Value = i;
            }
        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            long id;
            if ((Int64.TryParse(textBox8.Text, out id)))
            {
                button6.Enabled = true;
            }
            else
            {
                button6.Enabled = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if ((textBox5.Text == "") || (textBox6.Text == "") || (textBox7.Text == ""))
            {
                return;
            }

            for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
            {
                if (dataGridView2[1, i].Value.ToString() == textBox5.Text)
                {
                    label12.Text = "Запись с таким рег. номером уже существует";
                    return;
                }
            }


            dataGridView2.Rows.Add(dataGridView2.Rows.Count, textBox5.Text, textBox6.Text.ToUpper(), textBox7.Text, dateTimePicker2.Value.ToShortDateString());
            dataGridView2.Sort(dataGridView2.Columns[2], ListSortDirection.Ascending);
            for (int i = 1; i < dataGridView2.Rows.Count; i++)
            {
                dataGridView2[0, i - 1].Value = i;
            }
            label12.Text = "";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int row_id = -1;
            if (dataGridView2.SelectedCells.Count > 0)
            {
                row_id = dataGridView2.SelectedCells[0].RowIndex;
            }
            else
            {
                return;
            }
            dataGridView2.Rows.RemoveAt(row_id);
            for (int i = 1; i < dataGridView2.Rows.Count; i++)
            {
                dataGridView2[0, i - 1].Value = i;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            DataGridViewCell cell = null;
            if (textBox8.Text != "")
            {
                for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
                {
                    if (dataGridView2[1, i].Value.ToString() == textBox8.Text)
                    {
                        cell = dataGridView2[0, i];
                        dataGridView2.CurrentCell = cell;
                        dataGridView2.Rows[i].Selected = true;
                        break;
                    }
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FileStream w = new FileStream("лицензии.txt", FileMode.Create, FileAccess.Write);
            TextWriter w_txt = new StreamWriter(w);
            string buf = null;
            w_txt.WriteLine("ОТОЗВАНЫ");
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                buf = dataGridView1[1, i].Value + "-" + dataGridView1[2, i].Value + "(" + dataGridView1[3, i].Value + "," +
                    dataGridView1[4, i].Value + ")";
                w_txt.WriteLine(buf);
            }
            w_txt.WriteLine("ПРИОСТАНОВЛЕНЫ");
            for (int i = 0; i < dataGridView2.Rows.Count - 1; i++)
            {
                buf = dataGridView2[1, i].Value + "-" + dataGridView2[2, i].Value + "(" + dataGridView2[3, i].Value + "," +
                    dataGridView2[4, i].Value + ")";
                w_txt.WriteLine(buf);
            }
            w_txt.Close();
            w.Close();
            this.Close();
        }
    }
}
