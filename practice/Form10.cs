﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.Office.Interop.Excel;

namespace practice
{
    public partial class Form10 : Form
    {
        public Form10()
        {
            InitializeComponent();
        }

        private void Form10_Load(object sender, EventArgs e)
        {
            FileStream r = new FileStream("лицензии.txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            TextReader r_txt = new StreamReader(r);
            bool f = true;
            string[] str_array = new string[4];
            string s;
            s = r_txt.ReadLine();
            if (s != "ОТОЗВАНЫ")
            {
                r_txt.Close();
                r.Close();
                return;
            }
            int i = 1;
            while (f)
            {
                try
                {
                    s = r_txt.ReadLine();
                    if (s == "ПРИОСТАНОВЛЕНЫ")
                    {
                        f = false;
                    }
                }
                catch
                {
                    f = false;
                }
            }

            if (s != "ПРИОСТАНОВЛЕНЫ")
            {
                r_txt.Close();
                r.Close();
                return;
            }

            f = true;
            i = 1;
            while (f)
            {
                try
                {
                    s = r_txt.ReadLine();
                    str_array[0] = s.Substring(0, s.IndexOf('-'));
                    s = s.Remove(0, s.IndexOf('-') + 1);
                    str_array[1] = s.Substring(0, s.LastIndexOf('('));
                    s = s.Remove(0, s.LastIndexOf('(') + 1);
                    str_array[2] = s.Substring(0, s.LastIndexOf(','));
                    s = s.Remove(0, s.LastIndexOf(',') + 1);
                    str_array[3] = s.Substring(0, s.LastIndexOf(')'));
                    dataGridView1.Rows.Add(i, str_array[0], str_array[1], str_array[2], str_array[3]);
                    i++;
                }
                catch
                {
                    f = false;
                }
            }
            r_txt.Close();
            r.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                dataGridView1.Sort(dataGridView1.Columns[1], ListSortDirection.Ascending);
            }
            else if (radioButton3.Checked)
            {
                dataGridView1.Sort(dataGridView1.Columns[4], ListSortDirection.Ascending);
            }
            Microsoft.Office.Interop.Excel.Application excel_otchet = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excel_otchet.Workbooks.Open(Directory.GetCurrentDirectory() + "\\Отзывы.xls");
            Worksheet ws = wb.Sheets[1];
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {
                ws.Cells[i + 5, 1] = i;
                ws.Cells[i + 5, 2] = dataGridView1[2, i].Value;
                ws.Cells[i + 5, 3] = dataGridView1[1, i].Value;
                ws.Cells[i + 5, 4] = dataGridView1[3, i].Value;
                ws.Cells[i + 5, 5] = dataGridView1[4, i].Value;
            }
            int ind = 4 + dataGridView1.Rows.Count;
            Range r = ws.get_Range("A" + 5, "E" + ind);
            r.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            r.WrapText = true;
            r.HorizontalAlignment = XlHAlign.xlHAlignLeft;
            r.VerticalAlignment = XlVAlign.xlVAlignCenter;
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            if (fold.SelectedPath == "")
            {
                wb.Close(SaveChanges: false);
                excel_otchet.Quit();
                wb = null;
                ws = null;
                excel_otchet = null;
                GC.Collect();
                return;
            }
            wb.SaveAs(fold.SelectedPath + "\\Отзывы и приостановления ПФО.xls");
            wb.Close(SaveChanges: false);
            excel_otchet.Quit();
            wb = null;
            ws = null;
            excel_otchet = null;
            GC.Collect();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
