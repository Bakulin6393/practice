﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace practice
{
    public partial class Form2 : Form
    {
        bool first_cond = false;
        bool second_cond = false;
        string[] files;
        FolderBrowserDialog dest;

        public Form2()
        {
            InitializeComponent();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            if (fold.SelectedPath=="")
            {
                return;
            }
            label6.Text = fold.SelectedPath;
            if (label6.Text.Length > 30)
            {
                label6.Text = label6.Text.Substring(0, 30) + "...";
            }
            files = Directory.GetFiles(fold.SelectedPath, "*.xml");
            textBox1.Text = files.Length.ToString();
            first_cond = true;
            if (second_cond)
            {
                button2.Enabled = true;
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            dest = new FolderBrowserDialog();
            dest.ShowDialog();
            label8.Text = dest.SelectedPath;
            if (dest.SelectedPath == "")
            {
                return;
            }
            if (label8.Text.Length > 30)
            {
                label8.Text = label8.Text.Substring(0, 30) + "...";
            }
            second_cond = true;
            if (first_cond)
            {
                button2.Enabled = true;
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }

        private void button6_Click(object sender, EventArgs e)
        {
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            XDocument otchet = new XDocument();
            List<string> errors = new List<string>();
            string date;
            string buf;
            int kv;
            bool fail = false;
            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    fail = false;
                    otchet = XDocument.Load(files[i]);
                    date = otchet.Element("company").Attribute("period").Value;
                    buf = date.Substring(date.LastIndexOf('.') + 1);//год
                    date = date.Remove(date.LastIndexOf('.'));//не год
                    date = date.Remove(0, date.IndexOf('.') + 1);
                    kv = Int32.Parse(date) - 1;
                    if (buf == comboBox1.Text)//совпадение года
                    {
                        date = dest.SelectedPath + "\\" + otchet.Element("company").Element("attributes").Element("name").Value;
                        date = date.Replace('"', ' ');
                        date = date.Replace('/', ' ');
                        date = date.Replace('?', ' ');
                        date = date.Replace('<', ' ');
                        date = date.Replace('>', ' ');
                        date = date.Replace('*', ' ');
                        date = date.Replace('|', ' ');
                        date = date.Replace(':', ' ');
                        date = date.Insert(1,":");

                        kv = kv / 3 + 1;
                        switch (kv)//совпадение кварталов
                        {
                            case 1: if (radioButton1.Checked)
                                {
                                    Directory.CreateDirectory(date);
                                    File.Copy(files[i], date + "\\1s.xml");
                                }
                                break;

                            case 2: if (radioButton2.Checked)
                                {
                                    Directory.CreateDirectory(date);
                                    File.Copy(files[i], date + "\\1s.xml");
                                }
                                break;

                            case 3: if (radioButton3.Checked)
                                {
                                    Directory.CreateDirectory(date);
                                    File.Copy(files[i], date + "\\1s.xml");
                                }
                                break;

                            case 4: if (radioButton4.Checked)
                                {

                                    Directory.CreateDirectory(date);
                                    File.Copy(files[i], date + "\\1s.xml");
                                }
                                break;

                            default:
                                fail = true;
                                break;
                        }
                    }
                    else
                    {
                        fail = true;
                    }
                    if (fail)
                    {
                        errors.Add(files[i] + "  дата не совпадает с отчетной");
                    }
                    
                }
                catch (Exception ex)
                {
                    errors.Add(files[i]+" "+ex.Message);
                }
            }

            if (errors.Count > 0)
            {
                File.WriteAllLines(dest.SelectedPath + @"\log.txt", errors);
            }
        }
    }
}
