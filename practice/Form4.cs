﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;

namespace practice
{
    public partial class Form4 : Form
    {
        FileDialog dial;
        List <string> files;
        string destination = null;
        public Form4()
        {
            InitializeComponent();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            dial.ShowDialog();
            label6.Text = dial.FileName;
            if (dial.FileName.Substring(dial.FileName.LastIndexOf('.') + 1) != "xml")
            {
                label6.Text = "Выберите xml файл";
                return;
            }
            if (label6.Text.Length > 40)
            {
                label6.Text = label6.Text.Substring(0, 40) + "...";
            }
            XDocument file = XDocument.Load(dial.FileName);
            if (richTextBox1.Text == "")
            {
                richTextBox1.AppendText(file.Element("company").Element("attributes").Element("name").Value);
            }
            else
            {
                richTextBox1.AppendText("\n" + file.Element("company").Element("attributes").Element("name").Value);
            }
            files.Add(dial.FileName);
            numericUpDown1.Value += 1;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label5.Text = fold.SelectedPath;
            if (label5.Text.Length > 40)
            {
                label5.Text = label5.Text.Substring(0, 40) + "...";
            }
            destination = fold.SelectedPath;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            files = new List<string>();
            dial = new OpenFileDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            files.Clear();
            numericUpDown1.Value = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((destination == null) || (destination == ""))
            {
                label5.Text = "Вы не выбрали папку создания кучи";
                return;
            }
            bool f;
            int num = 0;
            string s=null;
            for (int i = 0; i < files.Count; i++)
            {
                f = true;
                s = files[i].Substring(files[i].LastIndexOf('\\')+1);
                s = s.Remove(s.LastIndexOf('.'));
                while (f)
                {
                    if (File.Exists(destination + "\\" + s + num + ".xml"))
                    {
                        num++;
                    }
                    else
                    {
                        f = false;
                        File.Copy(files[i], destination + "\\" + s + num + ".xml");
                    }
                }
                
            }
            MessageBox.Show("Куча успешно создана");

        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > 0)
            {
                button2.Enabled = true;
            }
            else
            {
                button2.Enabled = false;
            }
        }
    }
}
