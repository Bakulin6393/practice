﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace practice
{
    public partial class Form11 : Form
    {
        List<string> files = new List<string>();

        public Form11()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 40)
            {
                label2.Text = label2.Text.Substring(0, 40) + "...";
            }
            if (fold.SelectedPath == "")
            {
                return;
            }
            string[] filenames = Directory.GetFiles(fold.SelectedPath, "*.xml");

            for (int i = 0; i < filenames.Length; i++)
            {
                files.Add(filenames[i]);
                numericUpDown1.Value += 1;
                if (richTextBox1.Text == "")
                {
                    richTextBox1.AppendText(filenames[i]);
                }
                else
                {
                    richTextBox1.AppendText("\n" + filenames[i]);
                }
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            files.Clear();
            numericUpDown1.Value = 0;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > 0)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            if (fold.SelectedPath == "")
            {
                return;
            }
            File.Copy("Регион.xls",fold.SelectedPath+"\\Регион.xls");
        }
    }
}
