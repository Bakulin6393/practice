﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace practice
{
    public partial class Form3 : Form
    {
        FileDialog dial;

        public Form3()
        {
            InitializeComponent();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            dial.ShowDialog();
            label5.Text = dial.FileName;
            if (label5.Text.Length >60)
            {
                label5.Text = label5.Text.Substring(0, 60) + "...";
            }
            if (dial.FileName.Substring(dial.FileName.LastIndexOf('.') + 1) != "xml")
            {
                label5.Text = "Выберите xml файл";
                return;
            }
            XDocument file = XDocument.Load(dial.FileName);
            if (richTextBox1.Text == "")
            {
                richTextBox1.AppendText(file.Element("company").Element("attributes").Element("name").Value);
            }
            else
            {
                richTextBox1.AppendText("\n" +file.Element("company").Element("attributes").Element("name").Value);
            }
            numericUpDown1.Value += 1;

        }

        private void Form3_Load(object sender, EventArgs e)
        {
            dial = new OpenFileDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            richTextBox2.Clear();
            numericUpDown1.Value = 0;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > 0)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            richTextBox2.Clear();
            string[] s = richTextBox1.Text.Split('\n');
            List<string> str = new List<string>();
            for (int i = 0; i < s.Length; i++)
            {
                if (str.IndexOf(s[i]) < 0)
                {

                    str.Add(s[i]);
                }
            }
            int count;
            for (int i = 0; i < str.Count; i++)
            {
                count = 0;
                for (int j = 0; j < s.Length; j++)
                {
                    if (str[i] == s[j])
                    {
                        count++;
                    }
                }
                if (count > 1)
                {
                    richTextBox2.AppendText(str[i]+"  "+count+" раз\n");
                }
            }
        }
    }
}
