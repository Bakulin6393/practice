﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using Microsoft.Office.Interop.Excel;


namespace practice
{
    public partial class Form8 : Form
    {
        public Form8()
        {
            InitializeComponent();
        }

        List<string> files = new List<string>();
        FolderBrowserDialog fold = new FolderBrowserDialog();

        private void button1_Click(object sender, EventArgs e)
        {
            fold = new FolderBrowserDialog();
            fold.ShowDialog();

            if (fold.SelectedPath == "")
            {
                return;
            }

            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 40)
            {
                label2.Text = label2.Text.Substring(0, 40) + "...";
            }


        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            XDocument doc = new XDocument();
            if (fold.SelectedPath == "")
            {
                return;
            }
            string[] otchety = Directory.GetFiles(fold.SelectedPath, "*.xml");

            for (int i = 0; i < otchety.Length; i++)
            {
                try
                {
                    doc = XDocument.Load(otchety[i]);
                    if (richTextBox1.Text == "")
                    {
                        richTextBox1.AppendText(doc.Element("company").Element("attributes").Element("name").Value);
                    }
                    else
                    {
                        richTextBox1.AppendText("\n" + doc.Element("company").Element("attributes").Element("name").Value);
                    }
                    files.Add(otchety[i]);
                    numericUpDown1.Value += 1;
                }
                catch
                {
                }                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label2.Text = "";
            files.Clear();
            richTextBox1.Clear();
            numericUpDown1.Value = 0;
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            FileStream r = new FileStream("регионы.txt", FileMode.OpenOrCreate, FileAccess.Read);
            TextReader r_txt = new StreamReader(r);
            string buf;
            bool f = true;
            while (f)
            {
                try
                {
                    buf = r_txt.ReadLine();
                    comboBox3.Items.Add(buf);
                }
                catch
                {
                    f = false;
                }
            }
            r_txt.Close();
            r.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application excel_otchet = new Microsoft.Office.Interop.Excel.Application();
            Workbook wb = excel_otchet.Workbooks.Open(Directory.GetCurrentDirectory()+"\\Общие сведения.xls");
            Worksheet ws = wb.Sheets[1];
            XDocument doc = new XDocument();
            string short_name;
            string date;
            string buf;
            int kv=0;
            if (comboBox3.Text == "Россия")
            {
                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {
                        doc = XDocument.Load(files[i]);

                        date = doc.Element("company").Attribute("period").Value;
                        buf = date.Substring(date.LastIndexOf('.') + 1);//год
                        date = date.Remove(date.LastIndexOf('.'));//не год
                        date = date.Remove(0, date.IndexOf('.') + 1);
                        switch (comboBox1.Text)
                        {
                            case "Первый квартал": if (kv > 3)
                                {
                                    continue;
                                }
                                break;

                            case "Первое полугодие": if (kv > 6)
                                {
                                    continue;
                                }
                                break;

                            case "Девять месяцев": if (kv > 9)
                                {
                                    continue;
                                }
                                break;
                        }
                        ws.Cells[i + 5, 1] = i + 1;
                        ws.Cells[i + 5, 2] = doc.Element("company").Element("attributes").Element("name").Value;
                        short_name = doc.Element("company").Element("attributes").Element("name").Value;
                        short_name = short_name.Remove(short_name.Length - 1);
                        short_name = short_name.Substring(short_name.LastIndexOf('"') + 1);
                        ws.Cells[i + 5, 3] = short_name;
                        ws.Cells[i + 5, 4] = doc.Element("company").Attribute("id").Value;
                        ws.Cells[i + 5, 5] = doc.Element("company").Element("attributes").Element("address").Value;
                        ws.Cells[i + 5, 6] = doc.Element("company").Element("attributes").Element("phone").Value;
                        ws.Cells[i + 5, 7] = doc.Element("company").Element("attributes").Element("e-mail").Value;
                        ws.Cells[i + 5, 8] = doc.Element("company").Element("attributes").Element("head").Element("name").Value;
                        string code = doc.Element("company").Element("attributes").Element("code").Value;
                        switch (code)
                        {
                            case "01":
                                ws.Cells[i + 5, 9] = "Страхование (за исключением ОСАГО) и, возможно, перестрахование";
                                break;

                            case "02":
                                ws.Cells[i + 5, 9] = "Страхование (в т.ч. ОСАГО) и, возможно, перестрахование";
                                break;

                            case "03":
                                ws.Cells[i + 5, 9] = "Только перестрахование";
                                break;

                            case "04":
                                ws.Cells[i + 5, 9] = "ОМС и ДМС, либо только ОМС";
                                break;

                            case "05":
                                ws.Cells[i + 5, 9] = "Взаимное страхование на основании устава ОВС, предусматривающего заключение договора страхования";
                                break;

                            case "06":
                                ws.Cells[i + 5, 9] = "Взаимное страхование на основании устава ОВС, не предусматривающего заключение договора страхования";
                                break;

                            default:
                                ws.Cells[i + 5, 9] = "Код вида страховой деятельности указан страховщиком неверно";
                                break;
                        }
                    }
                    catch
                    {
                    }
                }
                int ind = 4 + files.Count;
                Range r = ws.get_Range("A" + 5, "I" + ind);
                r.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                r.WrapText = true;
                r.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                r.VerticalAlignment = XlVAlign.xlVAlignCenter;
            }
            else
            {
                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {
                        doc = XDocument.Load(files[i]);

                        date = doc.Element("company").Attribute("period").Value;
                        buf = date.Substring(date.LastIndexOf('.') + 1);//год
                        date = date.Remove(date.LastIndexOf('.'));//не год
                        date = date.Remove(0, date.IndexOf('.') + 1);
                        switch (comboBox1.Text)
                        {
                            case "Первый квартал": if (kv > 3)
                                {
                                    continue;
                                }
                                break;

                            case "Первое полугодие": if (kv > 6)
                                {
                                    continue;
                                }
                                break;

                            case "Девять месяцев": if (kv > 9)
                                {
                                    continue;
                                }
                                break;
                        }

                        bool f = IsCurrentPlace(doc);
                        if (!f)
                        {
                            continue;
                        }
                        ws.Cells[i + 5, 1] = i + 1;
                        ws.Cells[i + 5, 2] = doc.Element("company").Element("attributes").Element("name").Value;
                        short_name = doc.Element("company").Element("attributes").Element("name").Value;
                        short_name = short_name.Remove(short_name.Length - 1);
                        short_name = short_name.Substring(short_name.LastIndexOf('"') + 1);
                        ws.Cells[i + 5, 3] = short_name;
                        ws.Cells[i + 5, 4] = doc.Element("company").Attribute("id").Value;
                        ws.Cells[i + 5, 5] = doc.Element("company").Element("attributes").Element("address").Value;
                        ws.Cells[i + 5, 6] = doc.Element("company").Element("attributes").Element("phone").Value;
                        ws.Cells[i + 5, 7] = doc.Element("company").Element("attributes").Element("e-mail").Value;
                        ws.Cells[i + 5, 8] = doc.Element("company").Element("attributes").Element("head").Element("name").Value;
                        string code = doc.Element("company").Element("attributes").Element("code").Value;
                        switch (code)
                        {
                            case "01":
                                ws.Cells[i + 5, 9] = "Страхование (за исключением ОСАГО) и, возможно, перестрахование";
                                break;

                            case "02":
                                ws.Cells[i + 5, 9] = "Страхование (в т.ч. ОСАГО) и, возможно, перестрахование";
                                break;

                            case "03":
                                ws.Cells[i + 5, 9] = "Только перестрахование";
                                break;

                            case "04":
                                ws.Cells[i + 5, 9] = "ОМС и ДМС, либо только ОМС";
                                break;

                            case "05":
                                ws.Cells[i + 5, 9] = "Взаимное страхование на основании устава ОВС, предусматривающего заключение договора страхования";
                                break;

                            case "06":
                                ws.Cells[i + 5, 9] = "Взаимное страхование на основании устава ОВС, не предусматривающего заключение договора страхования";
                                break;

                            default:
                                ws.Cells[i + 5, 9] = "Код вида страховой деятельности указан страховщиком неверно";
                                break;
                        }
                    }
                    catch
                    {
                    }
                }
                int ind = 4 + files.Count;
                Range r = ws.get_Range("A" + 5, "I" + ind);
                r.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                r.WrapText = true;
                r.HorizontalAlignment = XlHAlign.xlHAlignLeft;
                r.VerticalAlignment = XlVAlign.xlVAlignCenter;
            }


            fold.ShowDialog();
            if (fold.SelectedPath == "")
            {
                wb.Close(SaveChanges: false);
                excel_otchet.Quit();
                wb = null;
                ws = null;
                excel_otchet = null;
                GC.Collect();
                return;
            }
            wb.SaveAs(fold.SelectedPath + "\\Общие сведения_"+comboBox3.Text+".xls");
            wb.Close(SaveChanges:false);
            excel_otchet.Quit();
            wb = null;
            ws = null;
            excel_otchet = null;
            GC.Collect();
        }

        private bool IsCurrentPlace(XDocument doc)
        {
            IEnumerable<XElement> lists = doc.Element("company").Element("form").Elements("list");
            IEnumerator<XElement> iterator_list = lists.GetEnumerator();
            IEnumerable<XElement> items;
            IEnumerator<XElement> iterator_item;
            IEnumerable<XElement> cols;
            IEnumerator<XElement> iterator_cols;
            bool res = false;
            while (iterator_list.MoveNext())
            {
                items = iterator_list.Current.Elements("item");
                iterator_item = items.GetEnumerator();
                while (iterator_item.MoveNext())
                {
                    cols = iterator_item.Current.Elements("col");
                    iterator_cols = cols.GetEnumerator();
                    iterator_cols.MoveNext();
                    if (iterator_cols.Current.Value.ToUpper() == comboBox3.Text.ToUpper())
                    {
                        return true;
                    }
                }
            }
            return res;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > 0)
            {
                button3.Enabled = true;
            }
            else
            {
                button3.Enabled = false;
            }
        }
    }
}
