﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace practice
{
    public partial class Form66 : Form
    {
        public Form66()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 40)
            {
                label2.Text = label2.Text.Substring(0, 40) + "...";
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label9.Text = fold.SelectedPath;
            if (label9.Text.Length > 40)
            {
                label9.Text = label9.Text.Substring(0, 40) + "...";
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label15.Text = fold.SelectedPath;
            if (label15.Text.Length > 40)
            {
                label15.Text = label15.Text.Substring(0, 40) + "...";
            }
        }
    }
}
