﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace practice
{
    public partial class Form27 : Form
    {
        public Form27()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fold = new FolderBrowserDialog();
            fold.ShowDialog();
            label2.Text = fold.SelectedPath;
            if (label2.Text.Length > 20)
            {
                label2.Text = label2.Text.Substring(0, 20) + "...";
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }
    }
}
