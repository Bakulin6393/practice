﻿namespace practice
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.принятьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.проверитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьКучуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьПапкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистика1СToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.информацияПоВыбраннойКомпанииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщикаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщикаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.компаниИВоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщикаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщикаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщикаToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщикаToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.крупнейшиеКомпанииToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщикаToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщикаToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.крупнейшиеКомпанииToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщикаToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator19 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщикаToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прямоеСтрахованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.принятоеПерестрахованиеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.переданноеПерестрахованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.количествоДоговоровToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator20 = new System.Windows.Forms.ToolStripSeparator();
            this.произвольныеПоказателиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПроизвольныхПоказателейToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прямоеСтрахованиеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.принятоеПерестрахованиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.переданноеПерестрахованиеToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.количествоДоговоровToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator21 = new System.Windows.Forms.ToolStripSeparator();
            this.произвольныеПоказателиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПроизвольныхПоказателейToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.портфельПремийИВыплатToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator22 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщиклвToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейМедстраховщиковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator23 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщиковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.округаЗаключенияДоговровЗаПериодToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.портфельПремийИВыплатToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.уровеньВыплатToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator24 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиМедстраховщиковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.показателиМедстраховщиклвToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator25 = new System.Windows.Forms.ToolStripSeparator();
            this.показателиСхСтраховщиковToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.статистикаОПНToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.общиеСведенияОФилиалахToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справочникиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.реестрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отозванныеИПриостановленныеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator26 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator27 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator28 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.отчетыToolStripMenuItem,
            this.статистика1СToolStripMenuItem,
            this.статистикаОПНToolStripMenuItem,
            this.справочникиToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(641, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.принятьToolStripMenuItem,
            this.toolStripSeparator26,
            this.проверитьToolStripMenuItem,
            this.toolStripSeparator27,
            this.создатьКучуToolStripMenuItem,
            this.создатьПапкиToolStripMenuItem});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(63, 21);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // принятьToolStripMenuItem
            // 
            this.принятьToolStripMenuItem.Name = "принятьToolStripMenuItem";
            this.принятьToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.принятьToolStripMenuItem.Text = "Принять";
            this.принятьToolStripMenuItem.Click += new System.EventHandler(this.принятьToolStripMenuItem_Click);
            // 
            // проверитьToolStripMenuItem
            // 
            this.проверитьToolStripMenuItem.Name = "проверитьToolStripMenuItem";
            this.проверитьToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.проверитьToolStripMenuItem.Text = "Проверить отчеты на дубляж";
            this.проверитьToolStripMenuItem.Click += new System.EventHandler(this.проверитьToolStripMenuItem_Click);
            // 
            // создатьКучуToolStripMenuItem
            // 
            this.создатьКучуToolStripMenuItem.Name = "создатьКучуToolStripMenuItem";
            this.создатьКучуToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.создатьКучуToolStripMenuItem.Text = "Создать кучу";
            this.создатьКучуToolStripMenuItem.Click += new System.EventHandler(this.создатьКучуToolStripMenuItem_Click);
            // 
            // создатьПапкиToolStripMenuItem
            // 
            this.создатьПапкиToolStripMenuItem.Name = "создатьПапкиToolStripMenuItem";
            this.создатьПапкиToolStripMenuItem.Size = new System.Drawing.Size(253, 22);
            this.создатьПапкиToolStripMenuItem.Text = "Создать папки";
            this.создатьПапкиToolStripMenuItem.Click += new System.EventHandler(this.создатьПапкиToolStripMenuItem_Click);
            // 
            // статистика1СToolStripMenuItem
            // 
            this.статистика1СToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.информацияПоВыбраннойКомпанииToolStripMenuItem,
            this.toolStripSeparator1,
            this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem,
            this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem,
            this.toolStripSeparator2,
            this.компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem,
            this.компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem,
            this.toolStripSeparator3,
            this.компаниИВоToolStripMenuItem,
            this.компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem,
            this.toolStripSeparator4,
            this.компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem,
            this.компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem,
            this.toolStripSeparator5,
            this.компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem,
            this.компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem,
            this.toolStripSeparator6,
            this.округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem,
            this.округаЗаключенияДоговровЗаПериодToolStripMenuItem,
            this.toolStripSeparator7,
            this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem,
            this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem});
            this.статистика1СToolStripMenuItem.Name = "статистика1СToolStripMenuItem";
            this.статистика1СToolStripMenuItem.Size = new System.Drawing.Size(107, 21);
            this.статистика1СToolStripMenuItem.Text = "Статистика 1-С";
            // 
            // информацияПоВыбраннойКомпанииToolStripMenuItem
            // 
            this.информацияПоВыбраннойКомпанииToolStripMenuItem.Name = "информацияПоВыбраннойКомпанииToolStripMenuItem";
            this.информацияПоВыбраннойКомпанииToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.информацияПоВыбраннойКомпанииToolStripMenuItem.Text = "Информация по выбранной компании";
            this.информацияПоВыбраннойКомпанииToolStripMenuItem.Click += new System.EventHandler(this.информацияПоВыбраннойКомпанииToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(488, 6);
            // 
            // сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem
            // 
            this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem.Name = "сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem";
            this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem.Text = "Сведения о компаниях, зарегистрированных в регионе";
            this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem.Click += new System.EventHandler(this.сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem_Click);
            // 
            // списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem
            // 
            this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem.Name = "списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem";
            this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem.Text = "Список компаний, осущ-их деятельность в регионе";
            this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem.Click += new System.EventHandler(this.списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(488, 6);
            // 
            // компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem
            // 
            this.компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem,
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem,
            this.уровеньВыплатToolStripMenuItem,
            this.toolStripSeparator8,
            this.показателиМедстраховщикаToolStripMenuItem,
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem,
            this.toolStripSeparator9,
            this.показателиСхСтраховщикаToolStripMenuItem,
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem});
            this.компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem.Name = "компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem";
            this.компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem.Text = "КомпаниЯ во всех регионах России нарастающим итогом";
            // 
            // премииВыплатыПортфельЗаклдоговорыToolStripMenuItem
            // 
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem.Name = "премииВыплатыПортфельЗаклдоговорыToolStripMenuItem";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem.Text = "Премии, выплаты, портфель, закл.договоры";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem.Click += new System.EventHandler(this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem_Click);
            // 
            // динамикаПремийВыплатзаклдоговоровToolStripMenuItem
            // 
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem.Name = "динамикаПремийВыплатзаклдоговоровToolStripMenuItem";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem.Text = "Динамика премий, выплат,закл.договоров";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem.Click += new System.EventHandler(this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem_Click);
            // 
            // уровеньВыплатToolStripMenuItem
            // 
            this.уровеньВыплатToolStripMenuItem.Name = "уровеньВыплатToolStripMenuItem";
            this.уровеньВыплатToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.уровеньВыплатToolStripMenuItem.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиМедстраховщикаToolStripMenuItem
            // 
            this.показателиМедстраховщикаToolStripMenuItem.Name = "показателиМедстраховщикаToolStripMenuItem";
            this.показателиМедстраховщикаToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.показателиМедстраховщикаToolStripMenuItem.Text = "Показатели мед.страховщика";
            this.показателиМедстраховщикаToolStripMenuItem.Click += new System.EventHandler(this.показателиМедстраховщикаToolStripMenuItem_Click);
            // 
            // динамикаПоказателейМедстраховщикаToolStripMenuItem
            // 
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem.Name = "динамикаПоказателейМедстраховщикаToolStripMenuItem";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem.Text = "Динамика показателей мед.страховщика";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem.Click += new System.EventHandler(this.динамикаПоказателейМедстраховщикаToolStripMenuItem_Click);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиСхСтраховщикаToolStripMenuItem
            // 
            this.показателиСхСтраховщикаToolStripMenuItem.Name = "показателиСхСтраховщикаToolStripMenuItem";
            this.показателиСхСтраховщикаToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.показателиСхСтраховщикаToolStripMenuItem.Text = "Показатели с/х страховщика";
            this.показателиСхСтраховщикаToolStripMenuItem.Click += new System.EventHandler(this.показателиСхСтраховщикаToolStripMenuItem_Click);
            // 
            // динамикаПоказателейСхСтраховщикаToolStripMenuItem
            // 
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem.Name = "динамикаПоказателейСхСтраховщикаToolStripMenuItem";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem.Text = "Динамика показателей с/х страховщика";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem.Click += new System.EventHandler(this.динамикаПоказателейСхСтраховщикаToolStripMenuItem_Click);
            // 
            // компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem
            // 
            this.компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1,
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1,
            this.уровеньВыплатToolStripMenuItem1,
            this.toolStripSeparator10,
            this.показателиМедстраховщикаToolStripMenuItem1,
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem1,
            this.toolStripSeparator11,
            this.показателиСхСтраховщикаToolStripMenuItem1,
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem1});
            this.компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem.Name = "компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem";
            this.компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem.Text = "КомпаниЯ во всех регионах России за период";
            // 
            // премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1
            // 
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1.Name = "премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1.Text = "Премии, выплаты, портфель, закл.договоры";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1.Click += new System.EventHandler(this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1_Click);
            // 
            // динамикаПремийВыплатзаклдоговоровToolStripMenuItem1
            // 
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1.Name = "динамикаПремийВыплатзаклдоговоровToolStripMenuItem1";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1.Text = "Динамика премий, выплат,закл.договоров";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1.Click += new System.EventHandler(this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem1_Click);
            // 
            // уровеньВыплатToolStripMenuItem1
            // 
            this.уровеньВыплатToolStripMenuItem1.Name = "уровеньВыплатToolStripMenuItem1";
            this.уровеньВыплатToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.уровеньВыплатToolStripMenuItem1.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem1.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem1_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиМедстраховщикаToolStripMenuItem1
            // 
            this.показателиМедстраховщикаToolStripMenuItem1.Name = "показателиМедстраховщикаToolStripMenuItem1";
            this.показателиМедстраховщикаToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.показателиМедстраховщикаToolStripMenuItem1.Text = "Показатели мед.страховщика";
            this.показателиМедстраховщикаToolStripMenuItem1.Click += new System.EventHandler(this.показателиМедстраховщикаToolStripMenuItem1_Click);
            // 
            // динамикаПоказателейМедстраховщикаToolStripMenuItem1
            // 
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem1.Name = "динамикаПоказателейМедстраховщикаToolStripMenuItem1";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem1.Text = "Динамика показателей мед.страховщика";
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиСхСтраховщикаToolStripMenuItem1
            // 
            this.показателиСхСтраховщикаToolStripMenuItem1.Name = "показателиСхСтраховщикаToolStripMenuItem1";
            this.показателиСхСтраховщикаToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.показателиСхСтраховщикаToolStripMenuItem1.Text = "Показатели с/х страховщика";
            this.показателиСхСтраховщикаToolStripMenuItem1.Click += new System.EventHandler(this.показателиСхСтраховщикаToolStripMenuItem1_Click);
            // 
            // динамикаПоказателейСхСтраховщикаToolStripMenuItem1
            // 
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem1.Name = "динамикаПоказателейСхСтраховщикаToolStripMenuItem1";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem1.Text = "Динамика показателей с/х страховщика";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(488, 6);
            // 
            // компаниИВоToolStripMenuItem
            // 
            this.компаниИВоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2,
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2,
            this.уровеньВыплатToolStripMenuItem2,
            this.toolStripSeparator12,
            this.показателиМедстраховщикаToolStripMenuItem2,
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem2,
            this.toolStripSeparator13,
            this.показателиСхСтраховщикаToolStripMenuItem2,
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2});
            this.компаниИВоToolStripMenuItem.Name = "компаниИВоToolStripMenuItem";
            this.компаниИВоToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниИВоToolStripMenuItem.Text = "КомпаниИ во всех регионах России нарастающим итогом";
            // 
            // премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2
            // 
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2.Name = "премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2.Text = "Премии, выплаты, портфель, закл.договоры";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2.Click += new System.EventHandler(this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2_Click);
            // 
            // динамикаПремийВыплатзаклдоговоровToolStripMenuItem2
            // 
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2.Name = "динамикаПремийВыплатзаклдоговоровToolStripMenuItem2";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2.Text = "Динамика премий, выплат,закл.договоров";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2.Click += new System.EventHandler(this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem2_Click);
            // 
            // уровеньВыплатToolStripMenuItem2
            // 
            this.уровеньВыплатToolStripMenuItem2.Name = "уровеньВыплатToolStripMenuItem2";
            this.уровеньВыплатToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.уровеньВыплатToolStripMenuItem2.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem2.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem2_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиМедстраховщикаToolStripMenuItem2
            // 
            this.показателиМедстраховщикаToolStripMenuItem2.Name = "показателиМедстраховщикаToolStripMenuItem2";
            this.показателиМедстраховщикаToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.показателиМедстраховщикаToolStripMenuItem2.Text = "Показатели мед.страховщика";
            this.показателиМедстраховщикаToolStripMenuItem2.Click += new System.EventHandler(this.показателиМедстраховщикаToolStripMenuItem2_Click);
            // 
            // динамикаПоказателейМедстраховщикаToolStripMenuItem2
            // 
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem2.Name = "динамикаПоказателейМедстраховщикаToolStripMenuItem2";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem2.Text = "Динамика показателей мед.страховщика";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem2.Click += new System.EventHandler(this.динамикаПоказателейМедстраховщикаToolStripMenuItem2_Click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиСхСтраховщикаToolStripMenuItem2
            // 
            this.показателиСхСтраховщикаToolStripMenuItem2.Name = "показателиСхСтраховщикаToolStripMenuItem2";
            this.показателиСхСтраховщикаToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.показателиСхСтраховщикаToolStripMenuItem2.Text = "Показатели с/х страховщика";
            this.показателиСхСтраховщикаToolStripMenuItem2.Click += new System.EventHandler(this.показателиСхСтраховщикаToolStripMenuItem2_Click);
            // 
            // динамикаПоказателейСхСтраховщикаToolStripMenuItem2
            // 
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2.Name = "динамикаПоказателейСхСтраховщикаToolStripMenuItem2";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2.Text = "Динамика показателей с/х страховщика";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2.Click += new System.EventHandler(this.динамикаПоказателейСхСтраховщикаToolStripMenuItem2_Click);
            // 
            // компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem
            // 
            this.компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3,
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem3,
            this.уровеньВыплатToolStripMenuItem3,
            this.toolStripSeparator14,
            this.показателиМедстраховщикаToolStripMenuItem3,
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem3,
            this.toolStripSeparator15,
            this.показателиСхСтраховщикаToolStripMenuItem3,
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem3});
            this.компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem.Name = "компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem";
            this.компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem.Text = "КомпаниИ во всех регионах России за период";
            // 
            // премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3
            // 
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3.Name = "премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3.Text = "Премии, выплаты, портфель, закл.договоры";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3.Click += new System.EventHandler(this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3_Click);
            // 
            // динамикаПремийВыплатзаклдоговоровToolStripMenuItem3
            // 
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem3.Name = "динамикаПремийВыплатзаклдоговоровToolStripMenuItem3";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem3.Text = "Динамика премий, выплат,закл.договоров";
            // 
            // уровеньВыплатToolStripMenuItem3
            // 
            this.уровеньВыплатToolStripMenuItem3.Name = "уровеньВыплатToolStripMenuItem3";
            this.уровеньВыплатToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.уровеньВыплатToolStripMenuItem3.Text = "Уровень выплат";
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиМедстраховщикаToolStripMenuItem3
            // 
            this.показателиМедстраховщикаToolStripMenuItem3.Name = "показателиМедстраховщикаToolStripMenuItem3";
            this.показателиМедстраховщикаToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.показателиМедстраховщикаToolStripMenuItem3.Text = "Показатели мед.страховщика";
            // 
            // динамикаПоказателейМедстраховщикаToolStripMenuItem3
            // 
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem3.Name = "динамикаПоказателейМедстраховщикаToolStripMenuItem3";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem3.Text = "Динамика показателей мед.страховщика";
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиСхСтраховщикаToolStripMenuItem3
            // 
            this.показателиСхСтраховщикаToolStripMenuItem3.Name = "показателиСхСтраховщикаToolStripMenuItem3";
            this.показателиСхСтраховщикаToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.показателиСхСтраховщикаToolStripMenuItem3.Text = "Показатели с/х страховщика";
            // 
            // динамикаПоказателейСхСтраховщикаToolStripMenuItem3
            // 
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem3.Name = "динамикаПоказателейСхСтраховщикаToolStripMenuItem3";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem3.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem3.Text = "Динамика показателей с/х страховщика";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(488, 6);
            // 
            // компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem
            // 
            this.компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4,
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4,
            this.уровеньВыплатToolStripMenuItem4,
            this.крупнейшиеКомпанииToolStripMenuItem,
            this.toolStripSeparator16,
            this.показателиМедстраховщикаToolStripMenuItem4,
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem4,
            this.toolStripSeparator17,
            this.показателиСхСтраховщикаToolStripMenuItem4,
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4});
            this.компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.Name = "компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem";
            this.компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.Text = "КомпаниИ по региону заключения договоров нарастающим итогом";
            // 
            // премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4
            // 
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4.Name = "премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4.Text = "Премии, выплаты, портфель, закл.договоры";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4.Click += new System.EventHandler(this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4_Click);
            // 
            // динамикаПремийВыплатзаклдоговоровToolStripMenuItem4
            // 
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4.Name = "динамикаПремийВыплатзаклдоговоровToolStripMenuItem4";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4.Text = "Динамика премий, выплат,закл.договоров";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4.Click += new System.EventHandler(this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem4_Click);
            // 
            // уровеньВыплатToolStripMenuItem4
            // 
            this.уровеньВыплатToolStripMenuItem4.Name = "уровеньВыплатToolStripMenuItem4";
            this.уровеньВыплатToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.уровеньВыплатToolStripMenuItem4.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem4.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem4_Click);
            // 
            // крупнейшиеКомпанииToolStripMenuItem
            // 
            this.крупнейшиеКомпанииToolStripMenuItem.Name = "крупнейшиеКомпанииToolStripMenuItem";
            this.крупнейшиеКомпанииToolStripMenuItem.Size = new System.Drawing.Size(342, 22);
            this.крупнейшиеКомпанииToolStripMenuItem.Text = "Крупнейшие компании";
            this.крупнейшиеКомпанииToolStripMenuItem.Click += new System.EventHandler(this.крупнейшиеКомпанииToolStripMenuItem_Click);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиМедстраховщикаToolStripMenuItem4
            // 
            this.показателиМедстраховщикаToolStripMenuItem4.Name = "показателиМедстраховщикаToolStripMenuItem4";
            this.показателиМедстраховщикаToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.показателиМедстраховщикаToolStripMenuItem4.Text = "Показатели мед.страховщика";
            this.показателиМедстраховщикаToolStripMenuItem4.Click += new System.EventHandler(this.показателиМедстраховщикаToolStripMenuItem4_Click);
            // 
            // динамикаПоказателейМедстраховщикаToolStripMenuItem4
            // 
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem4.Name = "динамикаПоказателейМедстраховщикаToolStripMenuItem4";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem4.Text = "Динамика показателей мед.страховщика";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem4.Click += new System.EventHandler(this.динамикаПоказателейМедстраховщикаToolStripMenuItem4_Click);
            // 
            // toolStripSeparator17
            // 
            this.toolStripSeparator17.Name = "toolStripSeparator17";
            this.toolStripSeparator17.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиСхСтраховщикаToolStripMenuItem4
            // 
            this.показателиСхСтраховщикаToolStripMenuItem4.Name = "показателиСхСтраховщикаToolStripMenuItem4";
            this.показателиСхСтраховщикаToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.показателиСхСтраховщикаToolStripMenuItem4.Text = "Показатели с/х страховщика";
            this.показателиСхСтраховщикаToolStripMenuItem4.Click += new System.EventHandler(this.показателиСхСтраховщикаToolStripMenuItem4_Click);
            // 
            // динамикаПоказателейСхСтраховщикаToolStripMenuItem4
            // 
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4.Name = "динамикаПоказателейСхСтраховщикаToolStripMenuItem4";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4.Text = "Динамика показателей с/х страховщика";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4.Click += new System.EventHandler(this.динамикаПоказателейСхСтраховщикаToolStripMenuItem4_Click);
            // 
            // компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem
            // 
            this.компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5,
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5,
            this.уровеньВыплатToolStripMenuItem5,
            this.крупнейшиеКомпанииToolStripMenuItem1,
            this.toolStripSeparator18,
            this.показателиМедстраховщикаToolStripMenuItem5,
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem5,
            this.toolStripSeparator19,
            this.показателиСхСтраховщикаToolStripMenuItem5,
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem5});
            this.компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem.Name = "компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem";
            this.компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem.Text = "КомпаниИ по региону заключения договоров за период";
            // 
            // премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5
            // 
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5.Name = "премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5.Text = "Премии, выплаты, портфель, закл.договоры";
            this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5.Click += new System.EventHandler(this.премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5_Click);
            // 
            // динамикаПремийВыплатзаклдоговоровToolStripMenuItem5
            // 
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5.Name = "динамикаПремийВыплатзаклдоговоровToolStripMenuItem5";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5.Text = "Динамика премий, выплат,закл.договоров";
            this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5.Click += new System.EventHandler(this.динамикаПремийВыплатзаклдоговоровToolStripMenuItem5_Click);
            // 
            // уровеньВыплатToolStripMenuItem5
            // 
            this.уровеньВыплатToolStripMenuItem5.Name = "уровеньВыплатToolStripMenuItem5";
            this.уровеньВыплатToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.уровеньВыплатToolStripMenuItem5.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem5.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem5_Click);
            // 
            // крупнейшиеКомпанииToolStripMenuItem1
            // 
            this.крупнейшиеКомпанииToolStripMenuItem1.Name = "крупнейшиеКомпанииToolStripMenuItem1";
            this.крупнейшиеКомпанииToolStripMenuItem1.Size = new System.Drawing.Size(342, 22);
            this.крупнейшиеКомпанииToolStripMenuItem1.Text = "Крупнейшие компании";
            this.крупнейшиеКомпанииToolStripMenuItem1.Click += new System.EventHandler(this.крупнейшиеКомпанииToolStripMenuItem1_Click);
            // 
            // toolStripSeparator18
            // 
            this.toolStripSeparator18.Name = "toolStripSeparator18";
            this.toolStripSeparator18.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиМедстраховщикаToolStripMenuItem5
            // 
            this.показателиМедстраховщикаToolStripMenuItem5.Name = "показателиМедстраховщикаToolStripMenuItem5";
            this.показателиМедстраховщикаToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.показателиМедстраховщикаToolStripMenuItem5.Text = "Показатели мед.страховщика";
            this.показателиМедстраховщикаToolStripMenuItem5.Click += new System.EventHandler(this.показателиМедстраховщикаToolStripMenuItem5_Click);
            // 
            // динамикаПоказателейМедстраховщикаToolStripMenuItem5
            // 
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem5.Name = "динамикаПоказателейМедстраховщикаToolStripMenuItem5";
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейМедстраховщикаToolStripMenuItem5.Text = "Динамика показателей мед.страховщика";
            // 
            // toolStripSeparator19
            // 
            this.toolStripSeparator19.Name = "toolStripSeparator19";
            this.toolStripSeparator19.Size = new System.Drawing.Size(339, 6);
            // 
            // показателиСхСтраховщикаToolStripMenuItem5
            // 
            this.показателиСхСтраховщикаToolStripMenuItem5.Name = "показателиСхСтраховщикаToolStripMenuItem5";
            this.показателиСхСтраховщикаToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.показателиСхСтраховщикаToolStripMenuItem5.Text = "Показатели с/х страховщика";
            this.показателиСхСтраховщикаToolStripMenuItem5.Click += new System.EventHandler(this.показателиСхСтраховщикаToolStripMenuItem5_Click);
            // 
            // динамикаПоказателейСхСтраховщикаToolStripMenuItem5
            // 
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem5.Name = "динамикаПоказателейСхСтраховщикаToolStripMenuItem5";
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem5.Size = new System.Drawing.Size(342, 22);
            this.динамикаПоказателейСхСтраховщикаToolStripMenuItem5.Text = "Динамика показателей с/х страховщика";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(488, 6);
            // 
            // компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem
            // 
            this.компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.прямоеСтрахованиеToolStripMenuItem,
            this.принятоеПерестрахованиеToolStripMenuItem1,
            this.переданноеПерестрахованиеToolStripMenuItem,
            this.количествоДоговоровToolStripMenuItem1,
            this.toolStripSeparator20,
            this.произвольныеПоказателиToolStripMenuItem,
            this.динамикаПроизвольныхПоказателейToolStripMenuItem1});
            this.компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem.Name = "компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem";
            this.компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem.Text = "КомпаниИ по региону регистрации нарастающим итогом";
            // 
            // прямоеСтрахованиеToolStripMenuItem
            // 
            this.прямоеСтрахованиеToolStripMenuItem.Name = "прямоеСтрахованиеToolStripMenuItem";
            this.прямоеСтрахованиеToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.прямоеСтрахованиеToolStripMenuItem.Text = "Прямое страхование";
            this.прямоеСтрахованиеToolStripMenuItem.Click += new System.EventHandler(this.прямоеСтрахованиеToolStripMenuItem_Click);
            // 
            // принятоеПерестрахованиеToolStripMenuItem1
            // 
            this.принятоеПерестрахованиеToolStripMenuItem1.Name = "принятоеПерестрахованиеToolStripMenuItem1";
            this.принятоеПерестрахованиеToolStripMenuItem1.Size = new System.Drawing.Size(304, 22);
            this.принятоеПерестрахованиеToolStripMenuItem1.Text = "Принятое перестрахование";
            this.принятоеПерестрахованиеToolStripMenuItem1.Click += new System.EventHandler(this.принятоеПерестрахованиеToolStripMenuItem1_Click);
            // 
            // переданноеПерестрахованиеToolStripMenuItem
            // 
            this.переданноеПерестрахованиеToolStripMenuItem.Name = "переданноеПерестрахованиеToolStripMenuItem";
            this.переданноеПерестрахованиеToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.переданноеПерестрахованиеToolStripMenuItem.Text = "Переданное перестрахование";
            this.переданноеПерестрахованиеToolStripMenuItem.Click += new System.EventHandler(this.переданноеПерестрахованиеToolStripMenuItem_Click);
            // 
            // количествоДоговоровToolStripMenuItem1
            // 
            this.количествоДоговоровToolStripMenuItem1.Name = "количествоДоговоровToolStripMenuItem1";
            this.количествоДоговоровToolStripMenuItem1.Size = new System.Drawing.Size(304, 22);
            this.количествоДоговоровToolStripMenuItem1.Text = "Количество договоров";
            this.количествоДоговоровToolStripMenuItem1.Click += new System.EventHandler(this.количествоДоговоровToolStripMenuItem1_Click);
            // 
            // toolStripSeparator20
            // 
            this.toolStripSeparator20.Name = "toolStripSeparator20";
            this.toolStripSeparator20.Size = new System.Drawing.Size(301, 6);
            // 
            // произвольныеПоказателиToolStripMenuItem
            // 
            this.произвольныеПоказателиToolStripMenuItem.Name = "произвольныеПоказателиToolStripMenuItem";
            this.произвольныеПоказателиToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.произвольныеПоказателиToolStripMenuItem.Text = "Произвольные показатели";
            this.произвольныеПоказателиToolStripMenuItem.Click += new System.EventHandler(this.произвольныеПоказателиToolStripMenuItem_Click);
            // 
            // динамикаПроизвольныхПоказателейToolStripMenuItem1
            // 
            this.динамикаПроизвольныхПоказателейToolStripMenuItem1.Name = "динамикаПроизвольныхПоказателейToolStripMenuItem1";
            this.динамикаПроизвольныхПоказателейToolStripMenuItem1.Size = new System.Drawing.Size(304, 22);
            this.динамикаПроизвольныхПоказателейToolStripMenuItem1.Text = "Динамика произвольных показателей";
            this.динамикаПроизвольныхПоказателейToolStripMenuItem1.Click += new System.EventHandler(this.динамикаПроизвольныхПоказателейToolStripMenuItem1_Click);
            // 
            // компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem
            // 
            this.компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.прямоеСтрахованиеToolStripMenuItem1,
            this.принятоеПерестрахованиеToolStripMenuItem,
            this.переданноеПерестрахованиеToolStripMenuItem1,
            this.количествоДоговоровToolStripMenuItem,
            this.toolStripSeparator21,
            this.произвольныеПоказателиToolStripMenuItem1,
            this.динамикаПроизвольныхПоказателейToolStripMenuItem});
            this.компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem.Name = "компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem";
            this.компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem.Text = "КомпаниИ по региону регистрации за период";
            // 
            // прямоеСтрахованиеToolStripMenuItem1
            // 
            this.прямоеСтрахованиеToolStripMenuItem1.Name = "прямоеСтрахованиеToolStripMenuItem1";
            this.прямоеСтрахованиеToolStripMenuItem1.Size = new System.Drawing.Size(304, 22);
            this.прямоеСтрахованиеToolStripMenuItem1.Text = "Прямое страхование";
            this.прямоеСтрахованиеToolStripMenuItem1.Click += new System.EventHandler(this.прямоеСтрахованиеToolStripMenuItem1_Click);
            // 
            // принятоеПерестрахованиеToolStripMenuItem
            // 
            this.принятоеПерестрахованиеToolStripMenuItem.Name = "принятоеПерестрахованиеToolStripMenuItem";
            this.принятоеПерестрахованиеToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.принятоеПерестрахованиеToolStripMenuItem.Text = "Принятое перестрахование";
            this.принятоеПерестрахованиеToolStripMenuItem.Click += new System.EventHandler(this.принятоеПерестрахованиеToolStripMenuItem_Click);
            // 
            // переданноеПерестрахованиеToolStripMenuItem1
            // 
            this.переданноеПерестрахованиеToolStripMenuItem1.Name = "переданноеПерестрахованиеToolStripMenuItem1";
            this.переданноеПерестрахованиеToolStripMenuItem1.Size = new System.Drawing.Size(304, 22);
            this.переданноеПерестрахованиеToolStripMenuItem1.Text = "Переданное перестрахование";
            this.переданноеПерестрахованиеToolStripMenuItem1.Click += new System.EventHandler(this.переданноеПерестрахованиеToolStripMenuItem1_Click);
            // 
            // количествоДоговоровToolStripMenuItem
            // 
            this.количествоДоговоровToolStripMenuItem.Name = "количествоДоговоровToolStripMenuItem";
            this.количествоДоговоровToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.количествоДоговоровToolStripMenuItem.Text = "Количество договоров";
            this.количествоДоговоровToolStripMenuItem.Click += new System.EventHandler(this.количествоДоговоровToolStripMenuItem_Click);
            // 
            // toolStripSeparator21
            // 
            this.toolStripSeparator21.Name = "toolStripSeparator21";
            this.toolStripSeparator21.Size = new System.Drawing.Size(301, 6);
            // 
            // произвольныеПоказателиToolStripMenuItem1
            // 
            this.произвольныеПоказателиToolStripMenuItem1.Name = "произвольныеПоказателиToolStripMenuItem1";
            this.произвольныеПоказателиToolStripMenuItem1.Size = new System.Drawing.Size(304, 22);
            this.произвольныеПоказателиToolStripMenuItem1.Text = "Произвольные показатели";
            // 
            // динамикаПроизвольныхПоказателейToolStripMenuItem
            // 
            this.динамикаПроизвольныхПоказателейToolStripMenuItem.Name = "динамикаПроизвольныхПоказателейToolStripMenuItem";
            this.динамикаПроизвольныхПоказателейToolStripMenuItem.Size = new System.Drawing.Size(304, 22);
            this.динамикаПроизвольныхПоказателейToolStripMenuItem.Text = "Динамика произвольных показателей";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(488, 6);
            // 
            // округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem
            // 
            this.округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem,
            this.портфельПремийИВыплатToolStripMenuItem1,
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem,
            this.уровеньВыплатToolStripMenuItem7,
            this.toolStripSeparator22,
            this.показателиМедстраховщиклвToolStripMenuItem,
            this.динамикаПоказателейМедстраховщиковToolStripMenuItem,
            this.toolStripSeparator23,
            this.показателиСхСтраховщиковToolStripMenuItem,
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1});
            this.округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.Name = "округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem";
            this.округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem.Text = "Округа заключения договоров нарастающим итогом";
            // 
            // премииВыплатыИлиЗаклДоговорыToolStripMenuItem
            // 
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem.Name = "премииВыплатыИлиЗаклДоговорыToolStripMenuItem";
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem.Text = "Премии, выплаты или закл. договоры";
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem.Click += new System.EventHandler(this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem_Click);
            // 
            // портфельПремийИВыплатToolStripMenuItem1
            // 
            this.портфельПремийИВыплатToolStripMenuItem1.Name = "портфельПремийИВыплатToolStripMenuItem1";
            this.портфельПремийИВыплатToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.портфельПремийИВыплатToolStripMenuItem1.Text = "Портфель премий и выплат";
            this.портфельПремийИВыплатToolStripMenuItem1.Click += new System.EventHandler(this.портфельПремийИВыплатToolStripMenuItem1_Click);
            // 
            // динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem
            // 
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem.Name = "динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem";
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem.Text = "Динамика премий, выплат или закл. договоров";
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem.Click += new System.EventHandler(this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem_Click);
            // 
            // уровеньВыплатToolStripMenuItem7
            // 
            this.уровеньВыплатToolStripMenuItem7.Name = "уровеньВыплатToolStripMenuItem7";
            this.уровеньВыплатToolStripMenuItem7.Size = new System.Drawing.Size(361, 22);
            this.уровеньВыплатToolStripMenuItem7.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem7.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem7_Click);
            // 
            // toolStripSeparator22
            // 
            this.toolStripSeparator22.Name = "toolStripSeparator22";
            this.toolStripSeparator22.Size = new System.Drawing.Size(358, 6);
            // 
            // показателиМедстраховщиклвToolStripMenuItem
            // 
            this.показателиМедстраховщиклвToolStripMenuItem.Name = "показателиМедстраховщиклвToolStripMenuItem";
            this.показателиМедстраховщиклвToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.показателиМедстраховщиклвToolStripMenuItem.Text = "Показатели мед.страховщиков";
            this.показателиМедстраховщиклвToolStripMenuItem.Click += new System.EventHandler(this.показателиМедстраховщиклвToolStripMenuItem_Click);
            // 
            // динамикаПоказателейМедстраховщиковToolStripMenuItem
            // 
            this.динамикаПоказателейМедстраховщиковToolStripMenuItem.Name = "динамикаПоказателейМедстраховщиковToolStripMenuItem";
            this.динамикаПоказателейМедстраховщиковToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.динамикаПоказателейМедстраховщиковToolStripMenuItem.Text = "Динамика показателей мед.страховщиков";
            this.динамикаПоказателейМедстраховщиковToolStripMenuItem.Click += new System.EventHandler(this.динамикаПоказателейМедстраховщиковToolStripMenuItem_Click);
            // 
            // toolStripSeparator23
            // 
            this.toolStripSeparator23.Name = "toolStripSeparator23";
            this.toolStripSeparator23.Size = new System.Drawing.Size(358, 6);
            // 
            // показателиСхСтраховщиковToolStripMenuItem
            // 
            this.показателиСхСтраховщиковToolStripMenuItem.Name = "показателиСхСтраховщиковToolStripMenuItem";
            this.показателиСхСтраховщиковToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.показателиСхСтраховщиковToolStripMenuItem.Text = "Показатели с/х страховщиков";
            this.показателиСхСтраховщиковToolStripMenuItem.Click += new System.EventHandler(this.показателиСхСтраховщиковToolStripMenuItem_Click);
            // 
            // динамикаПоказателейСхСтраховщиковToolStripMenuItem1
            // 
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1.Name = "динамикаПоказателейСхСтраховщиковToolStripMenuItem1";
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1.Text = "Динамика показателей с/х страховщиков";
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1.Click += new System.EventHandler(this.динамикаПоказателейСхСтраховщиковToolStripMenuItem1_Click);
            // 
            // округаЗаключенияДоговровЗаПериодToolStripMenuItem
            // 
            this.округаЗаключенияДоговровЗаПериодToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1,
            this.портфельПремийИВыплатToolStripMenuItem,
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1,
            this.уровеньВыплатToolStripMenuItem6,
            this.toolStripSeparator24,
            this.показателиМедстраховщиковToolStripMenuItem,
            this.показателиМедстраховщиклвToolStripMenuItem1,
            this.toolStripSeparator25,
            this.показателиСхСтраховщиковToolStripMenuItem1,
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem});
            this.округаЗаключенияДоговровЗаПериодToolStripMenuItem.Name = "округаЗаключенияДоговровЗаПериодToolStripMenuItem";
            this.округаЗаключенияДоговровЗаПериодToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.округаЗаключенияДоговровЗаПериодToolStripMenuItem.Text = "Округа заключения договров за период";
            // 
            // премииВыплатыИлиЗаклДоговорыToolStripMenuItem1
            // 
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1.Name = "премииВыплатыИлиЗаклДоговорыToolStripMenuItem1";
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1.Text = "Премии, выплаты или закл. договоры";
            this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1.Click += new System.EventHandler(this.премииВыплатыИлиЗаклДоговорыToolStripMenuItem1_Click);
            // 
            // портфельПремийИВыплатToolStripMenuItem
            // 
            this.портфельПремийИВыплатToolStripMenuItem.Name = "портфельПремийИВыплатToolStripMenuItem";
            this.портфельПремийИВыплатToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.портфельПремийИВыплатToolStripMenuItem.Text = "Портфель премий и выплат";
            this.портфельПремийИВыплатToolStripMenuItem.Click += new System.EventHandler(this.портфельПремийИВыплатToolStripMenuItem_Click);
            // 
            // динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1
            // 
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1.Name = "динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1";
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1.Text = "Динамика премий, выплат или закл. договоров";
            this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1.Click += new System.EventHandler(this.динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1_Click);
            // 
            // уровеньВыплатToolStripMenuItem6
            // 
            this.уровеньВыплатToolStripMenuItem6.Name = "уровеньВыплатToolStripMenuItem6";
            this.уровеньВыплатToolStripMenuItem6.Size = new System.Drawing.Size(361, 22);
            this.уровеньВыплатToolStripMenuItem6.Text = "Уровень выплат";
            this.уровеньВыплатToolStripMenuItem6.Click += new System.EventHandler(this.уровеньВыплатToolStripMenuItem6_Click);
            // 
            // toolStripSeparator24
            // 
            this.toolStripSeparator24.Name = "toolStripSeparator24";
            this.toolStripSeparator24.Size = new System.Drawing.Size(358, 6);
            // 
            // показателиМедстраховщиковToolStripMenuItem
            // 
            this.показателиМедстраховщиковToolStripMenuItem.Name = "показателиМедстраховщиковToolStripMenuItem";
            this.показателиМедстраховщиковToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.показателиМедстраховщиковToolStripMenuItem.Text = "Показатели мед.страховщиков";
            // 
            // показателиМедстраховщиклвToolStripMenuItem1
            // 
            this.показателиМедстраховщиклвToolStripMenuItem1.Name = "показателиМедстраховщиклвToolStripMenuItem1";
            this.показателиМедстраховщиклвToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.показателиМедстраховщиклвToolStripMenuItem1.Text = "Динамика показателей мед.страховщиклв";
            // 
            // toolStripSeparator25
            // 
            this.toolStripSeparator25.Name = "toolStripSeparator25";
            this.toolStripSeparator25.Size = new System.Drawing.Size(358, 6);
            // 
            // показателиСхСтраховщиковToolStripMenuItem1
            // 
            this.показателиСхСтраховщиковToolStripMenuItem1.Name = "показателиСхСтраховщиковToolStripMenuItem1";
            this.показателиСхСтраховщиковToolStripMenuItem1.Size = new System.Drawing.Size(361, 22);
            this.показателиСхСтраховщиковToolStripMenuItem1.Text = "Показатели с/х страховщиков";
            // 
            // динамикаПоказателейСхСтраховщиковToolStripMenuItem
            // 
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem.Name = "динамикаПоказателейСхСтраховщиковToolStripMenuItem";
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem.Size = new System.Drawing.Size(361, 22);
            this.динамикаПоказателейСхСтраховщиковToolStripMenuItem.Text = "Динамика показателей с/х страховщиков";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(488, 6);
            // 
            // сведенияПоОтзывамИПриостановлениямToolStripMenuItem
            // 
            this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem.Name = "сведенияПоОтзывамИПриостановлениямToolStripMenuItem";
            this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem.Text = "Сведения по отзывам и приостановлениям";
            this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem.Click += new System.EventHandler(this.сведенияПоОтзывамИПриостановлениямToolStripMenuItem_Click);
            // 
            // взносыВыплатыКомпанийСОтзывомToolStripMenuItem
            // 
            this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem.Name = "взносыВыплатыКомпанийСОтзывомToolStripMenuItem";
            this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem.Size = new System.Drawing.Size(491, 22);
            this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem.Text = "Взносы, выплаты компаний с отзывом";
            this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem.Click += new System.EventHandler(this.взносыВыплатыКомпанийСОтзывомToolStripMenuItem_Click);
            // 
            // статистикаОПНToolStripMenuItem
            // 
            this.статистикаОПНToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.общиеСведенияОФилиалахToolStripMenuItem});
            this.статистикаОПНToolStripMenuItem.Name = "статистикаОПНToolStripMenuItem";
            this.статистикаОПНToolStripMenuItem.Size = new System.Drawing.Size(115, 21);
            this.статистикаОПНToolStripMenuItem.Text = "Статистика ОПН";
            // 
            // общиеСведенияОФилиалахToolStripMenuItem
            // 
            this.общиеСведенияОФилиалахToolStripMenuItem.Name = "общиеСведенияОФилиалахToolStripMenuItem";
            this.общиеСведенияОФилиалахToolStripMenuItem.Size = new System.Drawing.Size(385, 22);
            this.общиеСведенияОФилиалахToolStripMenuItem.Text = "Общие сведения о филиалах( представительствах )";
            this.общиеСведенияОФилиалахToolStripMenuItem.Click += new System.EventHandler(this.общиеСведенияОФилиалахToolStripMenuItem_Click);
            // 
            // справочникиToolStripMenuItem
            // 
            this.справочникиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.реестрToolStripMenuItem,
            this.toolStripSeparator28,
            this.отозванныеИПриостановленныеToolStripMenuItem});
            this.справочникиToolStripMenuItem.Name = "справочникиToolStripMenuItem";
            this.справочникиToolStripMenuItem.Size = new System.Drawing.Size(99, 21);
            this.справочникиToolStripMenuItem.Text = "Справочники";
            // 
            // реестрToolStripMenuItem
            // 
            this.реестрToolStripMenuItem.Name = "реестрToolStripMenuItem";
            this.реестрToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.реестрToolStripMenuItem.Text = "Реестр";
            this.реестрToolStripMenuItem.Click += new System.EventHandler(this.реестрToolStripMenuItem_Click);
            // 
            // отозванныеИПриостановленныеToolStripMenuItem
            // 
            this.отозванныеИПриостановленныеToolStripMenuItem.Name = "отозванныеИПриостановленныеToolStripMenuItem";
            this.отозванныеИПриостановленныеToolStripMenuItem.Size = new System.Drawing.Size(278, 22);
            this.отозванныеИПриостановленныеToolStripMenuItem.Text = "Отозванные и приостановленные";
            this.отозванныеИПриостановленныеToolStripMenuItem.Click += new System.EventHandler(this.отозванныеИПриостановленныеToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(53, 21);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator26
            // 
            this.toolStripSeparator26.Name = "toolStripSeparator26";
            this.toolStripSeparator26.Size = new System.Drawing.Size(250, 6);
            // 
            // toolStripSeparator27
            // 
            this.toolStripSeparator27.Name = "toolStripSeparator27";
            this.toolStripSeparator27.Size = new System.Drawing.Size(250, 6);
            // 
            // toolStripSeparator28
            // 
            this.toolStripSeparator28.Name = "toolStripSeparator28";
            this.toolStripSeparator28.Size = new System.Drawing.Size(275, 6);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(641, 221);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Сводная отчетность";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem принятьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem проверитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьКучуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьПапкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистика1СToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem информацияПоВыбраннойКомпанииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem списокКомпанийОсущихДеятельностьВРегионеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem компаниЯВоВсехРегионахРоссииНарастающимИтогомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыПортфельЗаклдоговорыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатзаклдоговоровToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщикаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem компаниЯВоВсехРегионахРоссииЗаПериодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыПортфельЗаклдоговорыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатзаклдоговоровToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщикаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщикаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщикаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщикаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem компаниИВоToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыПортфельЗаклдоговорыToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатзаклдоговоровToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщикаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщикаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщикаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщикаToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem компаниИВоВсехРегионахРоссииЗаПериодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыПортфельЗаклдоговорыToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатзаклдоговоровToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщикаToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщикаToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщикаToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщикаToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem компаниИПоРегионуЗаключенияДоговоровНарастающимИтогомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыПортфельЗаклдоговорыToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатзаклдоговоровToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem крупнейшиеКомпанииToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщикаToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщикаToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщикаToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщикаToolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem компаниИПоРегионуЗаключенияДоговоровЗаПериодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыПортфельЗаклдоговорыToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатзаклдоговоровToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem крупнейшиеКомпанииToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщикаToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщикаToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщикаToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщикаToolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem компаниИПоРегионуРегистрацииНарастающимИтогомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem прямоеСтрахованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem принятоеПерестрахованиеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem переданноеПерестрахованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem количествоДоговоровToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem произвольныеПоказателиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПроизвольныхПоказателейToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem компаниИПоРегионуРегистрацииЗаПериодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem прямоеСтрахованиеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem принятоеПерестрахованиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem переданноеПерестрахованиеToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem количествоДоговоровToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem произвольныеПоказателиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem динамикаПроизвольныхПоказателейToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem округаЗаключенияДоговоровНарастающимИтогомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыИлиЗаклДоговорыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem портфельПремийИВыплатToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщиклвToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейМедстраховщиковToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщиковToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщиковToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem округаЗаключенияДоговровЗаПериодToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem премииВыплатыИлиЗаклДоговорыToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem портфельПремийИВыплатToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem динамикаПремийВыплатИлиЗаклДоговоровToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem уровеньВыплатToolStripMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщиковToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem показателиМедстраховщиклвToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem показателиСхСтраховщиковToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem динамикаПоказателейСхСтраховщиковToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сведенияПоОтзывамИПриостановлениямToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem взносыВыплатыКомпанийСОтзывомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem статистикаОПНToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem общиеСведенияОФилиалахToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справочникиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem реестрToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отозванныеИПриостановленныеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem сведенияОКомпанияхЗарегистрированныхВРегионеToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator19;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator20;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator21;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator22;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator23;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator24;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator25;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator26;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator27;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator28;
    }
}

